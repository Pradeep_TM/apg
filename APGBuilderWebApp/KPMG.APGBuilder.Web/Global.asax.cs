﻿using KGS.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace KPMG.APGBuilder.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                    "Default",                                              // Route name
                    "{controller}/{action}",                           // URL with parameters
                    new { controller = "BusinessProcess", action = "Index" }  // Parameter defaults
            );
        }

        private void RegisterModelBindings()
        {
            ModelBinders.Binders.DefaultBinder = new EntityBinder();
        }

        protected void Application_Start()
        {
            RegisterModelBindings();
            AreaRegistration.RegisterAllAreas();
            RegisterRoutes(RouteTable.Routes);
        }

        void Session_Start(Object Sender, EventArgs e)
        {
            Session["DisplayName"] = "Thathigowdar, Pradeep";
            Session["UserName"] = "IN-PTHATHIGOWDAR";
        }

        void Session_End(Object Sender, EventArgs e)
        {
            Session["DisplayName"] = null;
        }
    }
}