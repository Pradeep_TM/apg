﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class ExecutionRiskController : GridEntityController<ExecutionRisk>
    {
        public ExecutionRiskController() : base(new ExecutionRiskManager()) { }

        public override ActionResult Index()
        {
            IList<ExecutionRisk> ExecutionRiskList = Manager.GetList();
            ViewBag.Controller = APGConstants.EXECUTION_RISK;
            return View(GetViewModel(ExecutionRiskList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }

        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            ExecutionRisk executionRisk = Manager.New();
            ExecutionRiskViewModel viewModel = new ExecutionRiskViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                executionRisk.Name = viewModel.Name;
                executionRisk.Description = viewModel.Description;
                executionRisk.IsActive = viewModel.IsActive;
                executionRisk.CreatedBy = Session["UserName"].ToString();
                executionRisk.CreatedOn = DateTime.Now;
                Manager.Create(executionRisk);
                viewModel.Id = executionRisk.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection form)
        {
            ExecutionRisk executionRisk = Manager.Read(id);
            ExecutionRiskViewModel viewModel = new ExecutionRiskViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                executionRisk.Name = viewModel.Name;
                executionRisk.Description = viewModel.Description;
                executionRisk.IsActive = viewModel.IsActive;
                executionRisk.ModifiedBy = Session["UserName"].ToString();
                executionRisk.ModifiedOn = DateTime.Now;
                Manager.Update(executionRisk);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }


        public override ActionResult Delete(long id)
        {
            ExecutionRisk executionRisk = Manager.Read(id);
            executionRisk.IsActive = false;
            Manager.Update(executionRisk);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            ExecutionRisk executionRisk = Manager.Read(id);
            executionRisk.IsActive = true;
            Manager.Update(executionRisk);
            return RedirectToAction(APGConstants.INDEX);
        }

        #region ExecutionRisk Industry Mapping
        private void CreateProjectForStandard(ExecutionRiskViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {
                        dataContext.ExecutionRiskIndustryMappings.InsertOnSubmit(new ExecutionRiskIndustryMapping
                            {
                                ExecutionRiskId = model.Id,
                                SubIndustryId = selIndustry,
                                CreatedBy = Session["UserName"].ToString(),
                                CreatedOn = DateTime.Now
                            });

                        dataContext.SubmitChanges();
                    }
                }
            }
        }

        private void UpdateProjectForStandard(ExecutionRiskViewModel model)
        {
            var executionRisk = Manager.Read(model.Id);
            var existingIds = executionRisk.ExecutionRiskIndustryMappings.Select(exeRisk => exeRisk.SubIndustryId).Distinct().ToList();
            var subIndustryToBeDeleted = existingIds.Except(model.SelectedIndustry).ToList();
            var subIndustryToBeAdded = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (subIndustryToBeDeleted.Count > 0)
                {
                    var subIndustryMappings = dataContext.ExecutionRiskIndustryMappings.Where(exeRisk => exeRisk.ExecutionRiskId.Equals(model.Id) && subIndustryToBeDeleted.Contains(exeRisk.SubIndustryId)).ToList();
                    dataContext.ExecutionRiskIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                if (subIndustryToBeAdded.Count > 0)
                {
                    subIndustryToBeAdded.ForEach(selIndustry =>
                    dataContext.ExecutionRiskIndustryMappings.InsertOnSubmit(new ExecutionRiskIndustryMapping
                            {
                                ExecutionRiskId = model.Id,
                                SubIndustryId = selIndustry,
                                CreatedBy = Convert.ToString(Session["UserName"]),
                                CreatedOn = DateTime.Now
                            })
                    );
                }

                dataContext.SubmitChanges();
            }
        }
        #endregion

        protected override object GetViewModel(ExecutionRisk result)
        {
            return new ExecutionRiskViewModel(result);
        }

        protected override bool TryUpdateViewModel(ExecutionRisk entity)
        {
            return TryUpdateModel(new ExecutionRiskViewModel(entity));
        }

        protected override IEnumerable GetViewModel(IEnumerable<ExecutionRisk> results)
        {
            var executionRisks = results.Select(executionRisk => new ExecutionRiskViewModel(executionRisk)).ToArray();
            return executionRisks;
        }
    }
}
