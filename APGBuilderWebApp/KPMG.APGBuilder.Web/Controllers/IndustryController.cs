﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class IndustryController : GridEntityController<Industry>
    {
        public IndustryController() : base(new IndustryManager()) { }

        public override ActionResult Index()
        {
            IList<Industry> industryList = Manager.GetList();
            ViewBag.Controller = APGConstants.INDUSTRY;
            return View(GetViewModel(industryList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }

        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection form)
        {
            Industry industry = Manager.New();
            IndustryViewModel viewModel = new IndustryViewModel();

            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                industry.Name = viewModel.Name;
                industry.Description = viewModel.Description;
                industry.IsActive = viewModel.IsActive;
                industry.CreatedBy = Session["UserName"].ToString();
                industry.CreatedOn = DateTime.Now;
                industry.LOBId = viewModel.SelectedLobId;
                Manager.Create(industry);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection form)
        {
            Industry industry = Manager.Read(id);
            IndustryViewModel viewModel = new IndustryViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                industry.Name = viewModel.Name;
                industry.Description = viewModel.Description;
                industry.IsActive = viewModel.IsActive;
                industry.ModifiedBy = Session["UserName"].ToString();
                industry.ModifiedOn = DateTime.Now;
                industry.LOBId = viewModel.SelectedLobId;
                Manager.Update(industry);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            Industry industry = Manager.Read(id);
            industry.IsActive = false;
            Manager.Update(industry);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            Industry industry = Manager.Read(id);
            industry.IsActive = true;
            Manager.Update(industry);
            return RedirectToAction(APGConstants.INDEX);
        }

        protected override object GetViewModel(Industry result)
        {
            return new IndustryViewModel(result);
        }

        protected override bool TryUpdateViewModel(Industry entity)
        {
            return TryUpdateModel(new IndustryViewModel(entity));
        }

        protected override IEnumerable GetViewModel(IEnumerable<Industry> results)
        {
            List<IndustryViewModel> viewModels = new List<IndustryViewModel>();
            foreach (var industry in results)
            {
                IndustryViewModel viewModel = new IndustryViewModel();
                viewModel.Id = industry.Id;
                viewModel.Name = industry.Name;
                viewModel.Description = industry.Description;
                viewModel.IsActive = industry.IsActive;
                viewModel.SelectedLobId = industry.LOBId;
                viewModels.Add(viewModel);
            }
            return viewModels.ToArray();
        }
    }
}
