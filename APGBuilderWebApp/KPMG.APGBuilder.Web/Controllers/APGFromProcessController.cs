﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class APGFromProcessController : Controller
    {
        public ActionResult Create()
        {
            APGFromProcessViewModel viewModel = new APGFromProcessViewModel();
            return View(viewModel);
            //return View();
        }       
    }
}
