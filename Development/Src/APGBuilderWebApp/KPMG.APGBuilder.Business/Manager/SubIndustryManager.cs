﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class SubIndustryManager : EntityManager<Entity.SubIndustry>
    {
        public SubIndustryManager() : base() { }
        public SubIndustryManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public SubIndustryManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
