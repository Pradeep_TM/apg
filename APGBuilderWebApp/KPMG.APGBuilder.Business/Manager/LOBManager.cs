﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class LOBManager : EntityManager<Entity.LOB>
    {
        public LOBManager() : base() { }
        public LOBManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public LOBManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
