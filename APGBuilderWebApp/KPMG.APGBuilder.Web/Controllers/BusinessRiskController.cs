﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KGS.Framework.Business;
using KGS.Framework.Web.Mvc.Telerik;
using KPMG.APGBuilder.Web.Models;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class BusinessRiskController : GridEntityController<BusinessRisk>
    {
        //
        // GET: /BusinessRisk/

        public BusinessRiskController() : base(new BusinessRiskManager()) { }

        public override ActionResult Index()
        {

            IList<BusinessRisk> businessRiskList = Manager.GetList();
            ViewBag.Controller = APGConstants.BUSINESS_RISK;
            return View(GetViewModel(businessRiskList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }


        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            BusinessRisk bpObject = Manager.New();
            BusinessRiskViewModel viewModel = new BusinessRiskViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.CreatedBy = Session["UserName"].ToString();
                bpObject.CreatedOn = DateTime.Now;
                Manager.Create(bpObject);
                viewModel.Id = bpObject.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
                return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            BusinessRisk bpObject = Manager.Read(id);
            BusinessRiskViewModel viewModel = new BusinessRiskViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.ModifiedBy = Session["UserName"].ToString();
                bpObject.ModifiedOn = DateTime.Now;
                Manager.Update(bpObject);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            BusinessRisk bpObject = Manager.Read(id);
            bpObject.IsActive = false;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }
        public ActionResult Activate(long id)
        {
            BusinessRisk businessRisk = Manager.Read(id);
            businessRisk.IsActive = true;
            Manager.Update(businessRisk);
            return RedirectToAction(APGConstants.INDEX);
        }

        #region BusinessRisk Industry Mapping
        private void CreateProjectForStandard(BusinessRiskViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {

                        dataContext.BusinessRiskIndustryMappings.InsertOnSubmit(new BusinessRiskIndustryMapping
                        {
                            BusinessRiskId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Session["UserName"].ToString(),
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        private void UpdateProjectForStandard(BusinessRiskViewModel model)
        {
            var businessRisk = Manager.Read(model.Id);
            var existingIds = businessRisk.BusinessRiskIndustryMappings.Select(x => x.SubIndustryId).Distinct().ToList();
            var deletedSubIndustryIds = existingIds.Except(model.SelectedIndustry).ToList();
            var addedSubIndustryIds = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                //Delete the subindustry mappings which were unselected
                if (deletedSubIndustryIds.Count > 0)
                {
                    var subIndustryMappings = dataContext.BusinessRiskIndustryMappings.Where(x => x.BusinessRiskId == model.Id && deletedSubIndustryIds.Contains(x.SubIndustryId))
                                                                                    .ToList();
                    dataContext.BusinessRiskIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                //Add new sub industries
                if (addedSubIndustryIds.Count > 0)
                {
                    addedSubIndustryIds.ForEach(selIndustry =>
                    dataContext.BusinessRiskIndustryMappings.InsertOnSubmit(new BusinessRiskIndustryMapping
                    {
                        BusinessRiskId = model.Id,
                        SubIndustryId = selIndustry,
                        CreatedBy = Convert.ToString(Session["User"]),
                        CreatedOn = DateTime.Now
                    })
                    );
                }

                dataContext.SubmitChanges();
            }
        }

        #endregion BusinessProcess Industry Mapping

        protected override object GetViewModel(BusinessRisk result)

        {
            return new BusinessRiskViewModel(result);
        }

        protected override bool TryUpdateViewModel(BusinessRisk result)
        {
            return TryUpdateModel(new BusinessRiskViewModel(result));
        }


        protected override System.Collections.IEnumerable GetViewModel(IEnumerable<BusinessRisk> results)
        {
            var businessRisks = results.Select(businessRisk => new BusinessRiskViewModel(businessRisk)).ToArray();
            return businessRisks;
        }
    }
}
