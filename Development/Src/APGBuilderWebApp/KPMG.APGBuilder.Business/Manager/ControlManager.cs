﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class ControlManager:EntityManager<Entity.Control>
    {
        public ControlManager() : base() { }
        public ControlManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public ControlManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
