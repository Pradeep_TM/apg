﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace KPMG.APGBuilder.Web.Models
{
    public class SubIndustryViewModel
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public SubIndustryViewModel() { }
        public SubIndustryViewModel(SubIndustry subIndustry)
        {
            if(subIndustry!=null)
            {
                this.Id = subIndustry.Id;
                this.Name = subIndustry.Name;
                this.Description = subIndustry.Description;
                this.IsActive = subIndustry.IsActive;
                this.SelectedIndustryId = subIndustry.IndustryId;
            }

            this.subIndustry = subIndustry;
        }

        public SubIndustry subIndustry { get; set; }
        public long Id { get; set; }
        [Required(ErrorMessage = "SubIndustry Name is Required...")]
        public string Name { get; set; }
        [Required(ErrorMessage = "SubIndustry Description is Required...")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Select SubIndustry Status..."), DisplayName("Status")]
        public bool? IsActive { get; set; }

        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        public long SelectedIndustryId { get; set; }

        private Dictionary<long, string> industryDict = new Dictionary<long, string>();
        public Dictionary<long,string> IndustryDict
        {
            get
            {
                foreach(var industry in this.IndustryList)
                {
                    if(!industryDict.ContainsKey(industry.Id))
                    {
                        industryDict.Add(industry.Id, industry.Name);
                    }
                }
                return industryDict;
            }
        }

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if(_industryList==null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }
    }
}