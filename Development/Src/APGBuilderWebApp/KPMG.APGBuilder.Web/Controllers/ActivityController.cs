﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class ActivityController : GridEntityController<Activity>
    {
        public ActivityController() : base(new ActivityManager()) { }

        public override ActionResult Index()
        {
            IList<Activity> activityList = Manager.GetList();
            ViewBag.Controller = APGConstants.ACTIVITY;
            return View(GetViewModel(activityList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }

        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            Activity bpObject = Manager.New();
            ActivityViewModel viewModel = new ActivityViewModel();
            TryUpdateModel(viewModel);

            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.CreatedBy = Session[APGConstants.USER_NAME].ToString();
                bpObject.CreatedOn = DateTime.Now;
                Manager.Create(bpObject);
                viewModel.Id = bpObject.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            //return base.Edit(id);
            var viewModel = new ActivityViewModel(Manager.Read(id));
            return View(viewModel);
        }

        public ActionResult ChangeActivation(long id, bool isActive)
        {
            Activity spObject = Manager.Read(id);
            spObject.ModifiedBy = Session[APGConstants.USER_NAME].ToString();
            spObject.ModifiedOn = DateTime.Now;
            spObject.IsActive = !isActive;
            Manager.Update(spObject);
            return RedirectToAction(APGConstants.INDEX);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            Activity bpObject = Manager.Read(id);
            ActivityViewModel viewModel = new ActivityViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.ModifiedBy = Session[APGConstants.USER_NAME].ToString();
                bpObject.ModifiedOn = DateTime.Now;
                Manager.Update(bpObject);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            Activity bpObject = Manager.Read(id);
            bpObject.IsActive = false;
            Manager.Update(bpObject);
            return RedirectToAction("Index");
        }

        #region Activity Industry Mapping
        private void CreateProjectForStandard(ActivityViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {

                        dataContext.ActivityIndustryMappings.InsertOnSubmit(new ActivityIndustryMapping
                        {
                            ActivityId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Session["UserName"].ToString(),
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        private void UpdateProjectForStandard(ActivityViewModel model)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                var standardmapping = (from mapping in dataContext.ActivityIndustryMappings where mapping.ActivityId == model.Id select mapping).ToList();
                dataContext.ActivityIndustryMappings.DeleteAllOnSubmit(standardmapping);
                dataContext.SubmitChanges();
            }
            if (model.SelectedIndustry != null)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {
                        dataContext.ActivityIndustryMappings.InsertOnSubmit(new ActivityIndustryMapping
                        {
                            ActivityId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Session["UserName"].ToString(),
                            CreatedOn = DateTime.Now

                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        #endregion Activity Industry Mapping

        protected override object GetViewModel(Activity result)
        {
            return new ActivityViewModel(result);
        }

        protected override bool TryUpdateViewModel(Activity result)
        {
            return TryUpdateModel(new ActivityViewModel(result));
        }

        //protected override System.Collections.IEnumerable GetViewModel(IEnumerable<Activity> results)
        //{
        //    List<ActivityViewModel> viewModels = new List<ActivityViewModel>();
        //    foreach (var activity in results)
        //    {
        //        if (activity.IsActive == true)
        //        {
        //            ActivityViewModel viewModel = new ActivityViewModel();
        //            viewModel.Id = activity.Id;
        //            viewModel.Name = activity.Name;
        //            viewModel.Description = activity.Description;
        //            viewModel.IsActive = activity.IsActive;
        //            viewModels.Add(viewModel);
        //            if (activity.ActivityIndustryMappings != null && activity.ActivityIndustryMappings.Count > 0)
        //            {
        //                foreach (var industry in activity.ActivityIndustryMappings)
        //                {
        //                    if (string.IsNullOrEmpty(viewModel.SubIndustry))
        //                    {
        //                        viewModel.SubIndustry = industry.SubIndustry.Name;
        //                    }
        //                    else
        //                    {
        //                        viewModel.SubIndustry = viewModel.SubIndustry + "," + industry.SubIndustry.Name;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return viewModels.ToArray();
        //}

        protected override IEnumerable GetViewModel(IEnumerable<Activity> results)
        {
            var activities = results.Select(activity => new ActivityViewModel(activity)).ToArray();
            return activities;
        }
    }
}