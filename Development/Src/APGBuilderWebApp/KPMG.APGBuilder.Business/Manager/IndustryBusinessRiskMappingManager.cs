﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;


namespace KPMG.APGBuilder.Business.Manager
{
    public class IndustryBusinessRiskMappingManager : EntityManager<Entity.EnterpriseRiskIndustryMapping>
    {
        public IndustryBusinessRiskMappingManager() : base() { }
        public IndustryBusinessRiskMappingManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public IndustryBusinessRiskMappingManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
