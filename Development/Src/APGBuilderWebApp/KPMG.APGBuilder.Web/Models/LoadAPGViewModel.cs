﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KPMG.APGBuilder.Web.Models
{
    public class LoadAPGViewModel
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();

        public LoadAPGViewModel() { }

        public LoadAPGViewModel(APG APG)
        {
            if (APG != null)
            {
                this.Id = APG.Id;
                this.Name = APG.Name;
                this.Description = APG.Description;
                this.IsActive = APG.IsActive;
            }
            this.APG = APG;
        }

        public APG APG { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }



    }
}