﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class LoadAPGManager : EntityManager<Entity.APG>
    {
        public LoadAPGManager() : base() { }
        public LoadAPGManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public LoadAPGManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
