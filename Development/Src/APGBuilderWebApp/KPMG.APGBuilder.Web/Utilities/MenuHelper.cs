﻿using Kendo.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KPMG.APGBuilder.Web.Utilities
{
    public static class MenuHelper
    {
        
        public static void RegisterSiteMap()
        {
            if (!SiteMapManager.SiteMaps.ContainsKey("Web"))
            {
                SiteMapManager.SiteMaps.Register<XmlSiteMap>("Web", sitmap =>
                    sitmap.LoadFrom("~/Web.sitemap"));
            }
         
        }

     

    }
}