﻿using KPMG.APGBuilder.Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPMG.APGBuilder.Web.Models
{
    public interface IIndustySelector
    {
        long[] SelectedIndustry { get; set; }
        IList<SubIndustry> SubIndustryList { get; }
        IList<Industry> IndustryList { get; }
    }
}