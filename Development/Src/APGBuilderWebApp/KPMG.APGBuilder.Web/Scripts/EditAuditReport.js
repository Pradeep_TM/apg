﻿
function SendMailConfirm() {

    if (confirm("Are you sure you want to send mail?")) {
        // debugger;
        document.location = '<%=Url.Action("SendMailTo_Mangr", "IMConsolidate") %>';
        //return true;
    }
    else
        return false;

}

function TabClicked() {
    var $tabs = $("#tabContents").tabs();
    var selected = $tabs.tabs('option', 'selected'); // => 0
    //             alert(selected);
    //            //debugger;
    $("#SelectedTab").val(selected);
    return FnValidate();

}

function OnTabClick(objIsRel, selTab) {

    var prevTabText = $.trim($("#tabContents .ui-tabs-selected").text()); //Gets the text of previously selected Tab
    if (objIsRel == true)
        $('#btnMnSave').hide();
    else
        $('#btnMnSave').show();

    var $tabs = $("#tabContents").tabs();
    var selected = GetIndexOnTabName(selTab);
    $("#SelectedTab").val(selected);
    $tabs.tabs('select', parseInt(selected)); // switch to selected tab
    var isValid = true;
    //debugger;
    if ((selTab == "Release" || selTab == "Configuration") && prevTabText != selTab) {
        if (confirm("You want to save the record")) {
            if (prevTabText == "Release")
                $("#btnRel").click();
            else if (prevTabText == "Configuration")
                $("#btnPCA").click();

            isValid = true;
        }
        else
            isValid = true;
    }
    else if (selTab == "OTH" && (prevTabText == "Release" || prevTabText == "Configuration")) {
        if (confirm("You want to save the record")) {
            if (prevTabText == "Release")
                $("#btnRel").click();
            else if (prevTabText == "Configuration")
                $("#btnPCA").click();
            isValid = true;
        }
        else
            isValid = true;
    }


    return isValid;
}

function GetIndexOnTabName(selTab) {
    var tab = $('#tabContents a').filter(function () {
        return $(this).text() == selTab;
    }).parent();
    //debugger;
    var index = $("li", $('#tabContents').tabs()).index(tab);
    return index;
}

function FnValidate() {
    $(".follow").each(function () {
        var addon = $(this).closest('.t-alt'); // get the closest div.addon
        var objCatg = $('td.catg', addon).find('select');
        if ($(this).val() == '3' && objCatg.val() == '')//Not Followed is selected && category is blank
        {
            //debugger;
            alert('Category cannot be left blank for Non-Followed.');
            objCatg.focus();
            event.returnValue = false;
            return false;

        }

    });
}

function FnClicked(oRad, index, auditQuesId) {
    //$('#AuditQuestions_' + index + '__NCClassificationId').attr('selectedIndex', 0)
    $(function () {
        var CurrentFollowid = $('#AuditQuestions_' + index + '__CurrentFollowId').val();
        var QuesPCCTypeId = $('#AuditQuestions_' + index + '__QustPCCType').val();
        var Id = $('#AuditQuestions_' + index + '__FollowedId').val();
        var NCClassification = $('#AuditQuestions_' + index + '__NCClassificationId')
        var url = '<%: Url.Action("AsyncGetNCClassification", "IMConsolidate") %>';
        var objDdl = document.getElementById('AuditQuestions_' + index + '__NCClassificationId');
        $.getJSON(url, { followedId: Id, PCCTypeId: QuesPCCTypeId }, function (result) {
            var ddl = $(NCClassification);
            ddl.empty();

            //debugger;
            objDdl.style.width = '105px';
            //debugger;
            if ($(oRad).val() != '3') { //Not Non-Compliance
                ddl.append($('<option/>', { value: '' }).html(''));
            }

            $(result).each(function () {
                ddl.append(
                        $('<option/>', {
                            value: this.Value
                        }).html(this.Text)
                    );
            });
            var Responsevalue = $(oRad).val();
            if (Responsevalue == '3') {
                //objDdl.value = 1;
                $('#AuditQuestions_' + index + '__NCClassificationId').attr('disabled', false);
                $('#AuditQuestions_' + index + '__NCClassificationId').attr('selectedIndex', 0)
            }
            else if (Responsevalue == '2') {
                $('#AuditQuestions_' + index + '__NCClassificationId').attr('disabled', false);
                $('#AuditQuestions_' + index + '__NCClassificationId').attr('selectedIndex', 0)
                if (CurrentFollowid == 3) {
                    fnNCDetailstoUpdate(auditQuesId, index, Responsevalue);
                }
            }
            else {
                $('#AuditQuestions_' + index + '__NCClassificationId').attr('disabled', true);
                $('#AuditQuestions_' + index + '__NCClassificationId').attr('selectedIndex', 0)
                if (CurrentFollowid == 3) {
                    fnNCDetailstoUpdate(auditQuesId, index, Responsevalue);
                }
            }
        });
    });


}

function fnNCDetailstoUpdate(AuditQuestionId, indexValue, rating) {
    var url = '<%=Url.Action("UpdateNCStatus", "IMConsolidate", new { id = "-1", indexValue ="-2" }) %>';
    url = url.replace("-1", AuditQuestionId);
    url = url.replace("-2", indexValue);
    var retValue = window.showModalDialog(url, "UpdateNCDetails", "dialogWidth:800px; dialogHeight:500px; center:yes")
    //debugger;
    if (retValue == "No") {
        document.getElementById('AuditQuestions_' + indexValue + '__FollowedId').value =
                                document.getElementById('AuditQuestions_' + indexValue + '__CurrentFollowId').value
        var Id = document.getElementById('AuditQuestions_' + indexValue + '__FollowedId').value;
        var QuesPCCTypeId = document.getElementById('AuditQuestions_' + indexValue + '__QustPCCType').value;
        var NCClassification = $('#AuditQuestions_' + indexValue + '__NCClassificationId')
        $('#AuditQuestions_' + indexValue + '__NCClassificationId').attr('disabled', false);
        var url = '<%: Url.Action("AsyncGetNCClassification", "IMConsolidate") %>';
        var objDdl = document.getElementById('AuditQuestions_' + indexValue + '__NCClassificationId');
        $.getJSON(url, { followedId: Id, PCCTypeId: QuesPCCTypeId }, function (result) {
            var ddl = $(NCClassification);
            ddl.empty();
            objDdl.style.width = '105px';
            if (rating != "3")
                ddl.append($('<option/>', { value: '' }).html(''));

            $(result).each(function () {
                ddl.append(
                        $('<option/>', {
                            value: this.Value
                        }).html(this.Text)
                    );
            });
            objDdl.value = document.getElementById('AuditQuestions_' + indexValue + '__CurrentNCClassificationId').value;
        });

    }
    else if (retValue == "Yes") {
        document.getElementById('AuditQuestions_' + indexValue + '__CurrentNCClassificationId').value =
                                document.getElementById('AuditQuestions_' + indexValue + '__NCClassificationId').value;
        document.getElementById('AuditQuestions_' + indexValue + '__CurrentFollowId').value =
                                document.getElementById('AuditQuestions_' + indexValue + '__FollowedId').value
        document.getElementById('AuditQuestions_' + indexValue + '__Remarks').value = ""
    }
    return false;
}

function MarkNotApplicable(tabQuestionList) {
    var questionId = document.getElementById(tabQuestionList).value.split(',');
    for (var i = 0; i < questionId.length; i++) {
        if ($('#AuditQuestions_' + questionId[i] + '__FollowedId').val() != 3) {
            $('#AuditQuestions_' + questionId[i] + '__FollowedId').attr('selectedIndex', 1)
            $('#AuditQuestions_' + questionId[i] + '__NCClassificationId').attr('selectedIndex', 0)
            $('#AuditQuestions_' + questionId[i] + '__NCClassificationId').attr('disabled', true);
        }
    }
}


function BindNCCategoryDDLList(Responsevalue, objpccType, objDdl, addon, ncValue) {
    var url = '<%: Url.Action("AsyncGetNCClassification", "IMConsolidate") %>';
    $.getJSON(url, { followedId: Responsevalue, PCCTypeId: objpccType.value }, function (result) {
        var ddl = $('td.catg', addon).find('select');
        ddl.empty();
        // debugger;

        //debugger;
        objDdl.style.width = '105px';
        //debugger;
        if (Responsevalue != '3')
            ddl.append($('<option/>', { value: '' }).html(''));
        //debugger;
        $(result).each(function () {
            ddl.append(
                        $('<option/>', {
                            value: this.Value
                        }).html(this.Text)
                    );
        });
        objDdl.value = ncValue;
    });
}                       