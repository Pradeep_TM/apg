﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class SubIndustryController : GridEntityController<SubIndustry>
    {
        public SubIndustryController() : base(new SubIndustryManager()) { }

        public override ActionResult Index()
        {
            IList<SubIndustry> subIndustryList = Manager.GetList();
            ViewBag.Controller = APGConstants.SUB_INDUSTRY;
            return View(GetViewModel(subIndustryList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }

        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection form)
        {
            SubIndustry subIndustry = Manager.New();
            SubIndustryViewModel viewModel = new SubIndustryViewModel();
            TryUpdateModel(viewModel);
            if(ModelState.IsValid)
            {
                subIndustry.Name = viewModel.Name;
                subIndustry.Description = viewModel.Description;
                subIndustry.IsActive = viewModel.IsActive;
                subIndustry.CreatedBy = Session["UserName"].ToString();
                subIndustry.CreatedOn = DateTime.Now;
                subIndustry.IndustryId = viewModel.SelectedIndustryId;
                Manager.Create(subIndustry);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection form)
        {
            SubIndustry subIndustry = Manager.Read(id);
            SubIndustryViewModel viewModel = new SubIndustryViewModel();
            TryUpdateModel(viewModel);
            if(ModelState.IsValid)
            {
                subIndustry.Name = viewModel.Name;
                subIndustry.Description = viewModel.Description;
                subIndustry.IsActive = viewModel.IsActive;
                subIndustry.ModifiedBy = Session["UserName"].ToString();
                subIndustry.ModifiedOn = DateTime.Now;
                subIndustry.IndustryId = viewModel.SelectedIndustryId;
                Manager.Update(subIndustry);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            SubIndustry subIndustry = Manager.Read(id);
            subIndustry.IsActive = false;
            Manager.Update(subIndustry);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            SubIndustry subIndustry = Manager.Read(id);
            subIndustry.IsActive = true;
            Manager.Update(subIndustry);
            return RedirectToAction(APGConstants.INDEX);
        }

        protected override object GetViewModel(SubIndustry result)
        {
            return new SubIndustryViewModel(result);
        }

        protected override bool TryUpdateViewModel(SubIndustry entity)
        {
            return TryUpdateModel(new SubIndustryViewModel(entity));
        }

        protected override IEnumerable GetViewModel(IEnumerable<SubIndustry> results)
        {
            List<SubIndustryViewModel> viewModels = new List<SubIndustryViewModel>();
            foreach(var subIndustry in results)
            {
                SubIndustryViewModel viewModel = new SubIndustryViewModel();
                viewModel.Id = subIndustry.Id;
                viewModel.Name = subIndustry.Name;
                viewModel.Description = subIndustry.Description;
                viewModel.IsActive = subIndustry.IsActive;
                viewModel.SelectedIndustryId = subIndustry.IndustryId;
                viewModels.Add(viewModel);
            }
            return viewModels.ToArray();
        }

    }
}
