﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class ProcessController : GridEntityController<Process>
    {
        public ProcessController() : base(new ProcessManager()) { }

        public override ActionResult Index()
        {
            IList<Process> ProcessList = Manager.GetList();
            ViewBag.Controller = APGConstants.PROCESS;
            return View(GetViewModel(ProcessList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }


        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            Process bpObject = Manager.New();
            ProcessViewModel viewModel = new ProcessViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.CreatedBy = Session["UserName"].ToString();
                bpObject.CreatedOn = DateTime.Now;
                Manager.Create(bpObject);
                viewModel.Id = bpObject.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            Process bpObject = Manager.Read(id);
            ProcessViewModel viewModel = new ProcessViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.ModifiedBy = Session["UserName"].ToString();
                bpObject.ModifiedOn = DateTime.Now;
                Manager.Update(bpObject);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }


        public override ActionResult Delete(long id)
        {
            Process bpObject = Manager.Read(id);
            bpObject.IsActive = false;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            Process bpObject = Manager.Read(id);
            bpObject.IsActive = true;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }

        #region Process Industry Mapping
        private void CreateProjectForStandard(ProcessViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {
                        dataContext.ProcessIndustryMappings.InsertOnSubmit(new ProcessIndustryMapping
                        {
                            ProcessId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Session["UserName"].ToString(),
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        private void UpdateProjectForStandard(ProcessViewModel model)
        {
            var process = Manager.Read(model.Id);
            var existingIds = process.ProcessIndustryMappings.Select(proc => proc.SubIndustryId).Distinct().ToList();

            if (model.SelectedIndustry == null)
            {
                // if the industry mapping is not available
                model.SelectedIndustry = new List<long>().ToArray();
            }

            var subIndustryToBeDeleted = existingIds.Except(model.SelectedIndustry).ToList();
            var subIndustryToBeAdded = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (subIndustryToBeDeleted.Count > 0)
                {
                    var subIndustryMappings = dataContext.ProcessIndustryMappings.Where(proc => proc.ProcessId.Equals(model.Id) && subIndustryToBeDeleted.Contains(proc.SubIndustryId)).ToList();
                    dataContext.ProcessIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                if (subIndustryToBeAdded.Count > 0)
                {
                    subIndustryToBeAdded.ForEach(selIndustry =>
                    dataContext.ProcessIndustryMappings.InsertOnSubmit(new ProcessIndustryMapping
                    {
                        ProcessId = model.Id,
                        SubIndustryId = selIndustry,
                        CreatedBy = Convert.ToString(Session["UserName"]),
                        CreatedOn = DateTime.Now
                    })
                    );
                }

                dataContext.SubmitChanges();
            }
        }

        #endregion Process Industry Mapping


        protected override object GetViewModel(Process result)
        {
            return new ProcessViewModel(result);
        }

        protected override bool TryUpdateViewModel(Process result)
        {
            return TryUpdateModel(new ProcessViewModel(result));
        }

        protected override System.Collections.IEnumerable GetViewModel(IEnumerable<Process> results)
        {
            var processes = results.Select(process => new ProcessViewModel(process)).ToArray();
            return processes;
        }



    }
}
