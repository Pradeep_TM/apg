﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class EnterpriseRiskManager : EntityManager<Entity.EnterpriseRisk>
    {
        public EnterpriseRiskManager() : base() { }
        public EnterpriseRiskManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public EnterpriseRiskManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
