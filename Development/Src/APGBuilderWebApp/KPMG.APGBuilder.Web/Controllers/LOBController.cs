﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class LOBController : GridEntityController<LOB>
    {
        public LOBController() : base(new LOBManager()) { }


        public override ActionResult Index()
        {
            IList<LOB> lobList = Manager.GetList();
            ViewBag.Controller = APGConstants.LOB;
            return View(GetViewModel(lobList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }

        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection form)
        {
            LOB lob = Manager.New();
            LOBViewModel viewModel = new LOBViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                lob.Name = viewModel.Name;
                lob.Description = viewModel.Description;
                lob.IsActive = viewModel.IsActive;
                lob.CreatedBy = Session["UserName"].ToString();
                lob.CreatedOn = DateTime.Now;
                Manager.Create(lob);
                viewModel.Id = lob.Id; // gets the ID of the LOB
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection form)
        {
            LOB lob = Manager.Read(id);
            LOBViewModel viewModel = new LOBViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                lob.Name = viewModel.Name;
                lob.Description = viewModel.Description;
                lob.IsActive = viewModel.IsActive;
                lob.ModifiedBy = Session["UserName"].ToString();
                lob.ModifiedOn = DateTime.Now;
                Manager.Update(lob);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            LOB lob = Manager.Read(id);
            lob.IsActive = false;
            Manager.Update(lob);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            LOB lob = Manager.Read(id);
            lob.IsActive = true;
            Manager.Update(lob);
            return RedirectToAction(APGConstants.INDEX);
        }

        protected override object GetViewModel(LOB result)
        {
            return new LOBViewModel(result);
        }

        protected override IEnumerable GetViewModel(IEnumerable<LOB> results)
        {
            List<LOBViewModel> viewModels = new List<LOBViewModel>();
            foreach (var lob in results)
            {
                LOBViewModel viewModel = new LOBViewModel();
                viewModel.Id = lob.Id;
                viewModel.Name = lob.Name;
                viewModel.Description = lob.Description;
                viewModel.IsActive = lob.IsActive;
                viewModels.Add(viewModel);
            }
            return viewModels.ToArray();
        }
    }
}
