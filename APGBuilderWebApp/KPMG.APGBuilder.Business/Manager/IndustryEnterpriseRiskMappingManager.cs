﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class IndustryEnterpriseRiskMappingManager : EntityManager<Entity.EnterpriseRiskIndustryMapping>
    {
        public IndustryEnterpriseRiskMappingManager() : base() { }
        public IndustryEnterpriseRiskMappingManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public IndustryEnterpriseRiskMappingManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
