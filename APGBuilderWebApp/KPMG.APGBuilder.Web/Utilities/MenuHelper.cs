﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KPMG.APGBuilder.Web.Utilities
{
    public static class MenuHelper
    {
        
        private static string GetBackLink(SiteMapNode parentNode)
        {
            return "<li class='li-back'><a href='" + parentNode.Url + "' title='" + parentNode.Title + "'></a></li>";
        }

        public static string MenuGeneration(this HtmlHelper helper)
        {
            var sb = new StringBuilder();
            sb.Append("<ul class='menu'>");
            var topLevelNodes = SiteMap.RootNode.ChildNodes;
            foreach (SiteMapNode node in topLevelNodes)
            {
                if (SiteMap.CurrentNode == node)
                    sb.AppendLine("<li class='selectedMenuItem'>");
                else
                    sb.AppendLine("<li>");
                sb.AppendFormat("<a href='{0}'>{1}</a>", node.Url, helper.Encode(node.Title));
                sb.AppendLine("</li>");
            }
            sb.Append("</ul>");
            return sb.ToString();
        }

    }
}