﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class SubProcessManager : EntityManager<Entity.SubProcess>
    {
        public SubProcessManager() : base() { }
        public SubProcessManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public SubProcessManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
