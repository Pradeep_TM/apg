﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class UserManager : EntityManager<Entity.User>
    {
        public UserManager() : base() { }
        public UserManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public UserManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
