﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class APGFromProcessController : Controller
    {
        public ActionResult Create()
        {
            APGFromProcessViewModel viewModel = new APGFromProcessViewModel();
            return View(viewModel);
            //return View();
        }

        public JsonResult ProcessList(string businessProcesses)
        {
            var businessProcessIds = businessProcesses.Split(';').Where(x => !string.IsNullOrEmpty(x)).Select(x => Convert.ToInt64(x.Replace("businessProcess_", ""))).ToList();
            var context = new APGBuilderModelDataContext();
            var processes = (from stringLib in context.StringLibraries
                             join proc in context.Processes on stringLib.ProcessId equals proc.Id
                             where businessProcessIds.Contains(stringLib.BusinessProcessId)
                             select new { proc.Id, proc.Name }).ToList().Distinct();
            return Json(processes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubProcessList(string processes)
        {
            var processIds = processes.Split(';').Where(x => !string.IsNullOrEmpty(x)).Select(x => Convert.ToInt64(x.Replace("process_", ""))).ToList();
            var context = new APGBuilderModelDataContext();
            var subProcesses = (from stringLib in context.StringLibraries
                                join subproc in context.SubProcesses on stringLib.SubProcessId equals subproc.Id
                                where processIds.Contains(stringLib.ProcessId)
                                select new { subproc.Id, subproc.Name }).ToList().Distinct();
            return Json(subProcesses, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult ActivityList(string subProcesses)
        //{
        //    var activityIds = subProcesses.Split(';').Where(x => !string.IsNullOrEmpty(x)).Select(x => Convert.ToInt64(x.Replace("subProcess_", ""))).ToList();
        //    var context = new APGBuilderModelDataContext();
        //    var activities = (from stringLib in context.StringLibraries
        //                      join activity in context.Activities on stringLib.ActivityId equals activity.Id
        //                      where activityIds.Contains(stringLib.ActivityId)
        //                      select new { activity.Id, activity.Name }).ToList().Distinct();
        //    return Json(activities, JsonRequestBehavior.AllowGet);
        //}

    }
}
