﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KGS.Framework.Business;
using KGS.Framework.Web.Mvc.Telerik;
using KPMG.APGBuilder.Web.Models;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class TestController : GridEntityController<Test>
    {
        //
        // GET: /Test/

        public TestController() : base(new TestManager()) { }

        //public ActionResult Index()
        //{
        //    TestViewModel viewModel = new TestViewModel();
        //    return View(viewModel);
        //}

        public override ActionResult Create()
        {
            TestViewModel viewModel = new TestViewModel();
            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            //TestViewModel viewModel = new TestViewModel();
            //return View(viewModel);

            var viewModel = new TestViewModel(Manager.Read(id));
            return View("Create", viewModel);
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            Test testObject = Manager.New();
            TestViewModel viewModel = new TestViewModel();
            TryUpdateModel(viewModel);

            if (ModelState.IsValid)
            {
                if (viewModel.Id == 0)
                {
                    testObject.Name = viewModel.Name;
                    testObject.Description = viewModel.Description;
                    testObject.Hypothesis = viewModel.Hypothesis;
                    testObject.Notes = viewModel.Notes;
                    testObject.Technique = viewModel.Technique;
                    testObject.System = viewModel.System;
                    testObject.SystemNotes = viewModel.SystemNotes;
                    testObject.EstimatedHours = (decimal)viewModel.EstimatedHours;
                    testObject.Approved = viewModel.Approved;
                    testObject.IsActive = viewModel.IsActive;
                    testObject.CreatedBy = Session[APGConstants.USER_NAME].ToString();
                    testObject.CreatedOn = DateTime.Now;
                    Manager.Create(testObject);
                    viewModel.Id = testObject.Id;
                    CreateTestSteps(viewModel);
                    return RedirectToAction("Create");
                }
                else
                {
                    Test bpObject = Manager.Read(viewModel.Id);
                    bpObject.Name = viewModel.Name;
                    bpObject.Description = viewModel.Description;
                    bpObject.Hypothesis = viewModel.Hypothesis;
                    bpObject.Notes = viewModel.Notes;
                    bpObject.Technique = viewModel.Technique;
                    bpObject.System = viewModel.System;
                    bpObject.SystemNotes = viewModel.SystemNotes;
                    bpObject.EstimatedHours = (decimal)viewModel.EstimatedHours;
                    bpObject.Approved = viewModel.Approved;
                    bpObject.IsActive = viewModel.IsActive;
                    bpObject.ModifiedBy = Session[APGConstants.USER_NAME].ToString();
                    bpObject.ModifiedOn = DateTime.Now;
                    Manager.Update(bpObject);
                    //UpdateProjectForStandard(viewModel);
                    //return RedirectToAction(APGConstants.INDEX);
                    return RedirectToAction("Edit", new { id = viewModel.Id });

                }
            }

            return View(APGConstants.CREATE, viewModel);
        }


        #region Subprodess Industry Mapping
        private void CreateTestSteps(TestViewModel model)
        {
            if (model.TestStepList != null && model.TestStepList.Count > 0)
            {
                int i = 0;
                foreach (var testSteps in model.TestStepList)
                {

                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {
                        dataContext.TestSteps.InsertOnSubmit(new TestStep
                        {
                            TestId = model.Id,
                            Description = model.TestStepList[i].Description,
                            SortOrder = model.TestStepList[i].SortOrder,
                            CreatedBy = Session[APGConstants.USER_NAME].ToString(),
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                    i = i + 1;

                }
            }
        }

        #endregion


    }
}
