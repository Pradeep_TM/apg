﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class EnterpriseRiskController : GridEntityController<EnterpriseRisk>
    {
        //
        // GET: /EnterpriseRisk/

        public EnterpriseRiskController() : base(new EnterpriseRiskManager()) { }


        public override ActionResult Index()
        {
            IList<EnterpriseRisk> enterpriseRiskList = Manager.GetList();
            ViewBag.Controller = APGConstants.ENTERPRISE_RISK;
            return View(GetViewModel(enterpriseRiskList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }


        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            EnterpriseRisk bpObject = Manager.New();
            EnterpriseRiskViewModel viewModel = new EnterpriseRiskViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.CreatedBy = Session["UserName"].ToString();
                bpObject.CreatedOn = DateTime.Now;
                Manager.Create(bpObject);
                viewModel.Id = bpObject.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            EnterpriseRisk bpObject = Manager.Read(id);
            EnterpriseRiskViewModel viewModel = new EnterpriseRiskViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.ModifiedBy = Session["UserName"].ToString();
                bpObject.ModifiedOn = DateTime.Now;
                Manager.Update(bpObject);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            EnterpriseRisk bpObject = Manager.Read(id);
            bpObject.IsActive = false;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            EnterpriseRisk enterpriseRisk = Manager.Read(id);
            enterpriseRisk.IsActive = true;
            Manager.Update(enterpriseRisk);
            return RedirectToAction(APGConstants.INDEX);
        }

        #region EnterpriseRisk Industry Mapping
        private void CreateProjectForStandard(EnterpriseRiskViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {

                        dataContext.EnterpriseRiskIndustryMappings.InsertOnSubmit(new EnterpriseRiskIndustryMapping
                        {
                            EnterpriseRiskId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Session["UserName"].ToString(),
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        private void UpdateProjectForStandard(EnterpriseRiskViewModel model)
        {
            var enterpriseRisk = Manager.Read(model.Id);
            var existingIds = enterpriseRisk.EnterpriseRiskIndustryMappings.Select(x => x.SubIndustryId).Distinct().ToList();
            var deletedSubIndustryIds = existingIds.Except(model.SelectedIndustry).ToList();
            var addedSubIndustryIds = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                //Delete the subindustry mappings which were unselected
                if (deletedSubIndustryIds.Count > 0)
                {
                    var subIndustryMappings = dataContext.EnterpriseRiskIndustryMappings.Where(x => x.EnterpriseRiskId == model.Id && deletedSubIndustryIds.Contains(x.SubIndustryId))
                                                                                    .ToList();
                    dataContext.EnterpriseRiskIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                //Add new sub industries
                if (addedSubIndustryIds.Count > 0)
                {
                    addedSubIndustryIds.ForEach(selIndustry =>
                    dataContext.EnterpriseRiskIndustryMappings.InsertOnSubmit(new EnterpriseRiskIndustryMapping
                    {
                        EnterpriseRiskId = model.Id,
                        SubIndustryId = selIndustry,
                        CreatedBy = Convert.ToString(Session["User"]),
                        CreatedOn = DateTime.Now
                    })
                    );
                }

                dataContext.SubmitChanges();
            }
        }

        #endregion BusinessProcess Industry Mapping

        protected override object GetViewModel(EnterpriseRisk result)
        {
            return new EnterpriseRiskViewModel(result);
        }

        protected override bool TryUpdateViewModel(EnterpriseRisk result)
        {
            return TryUpdateModel(new EnterpriseRiskViewModel(result));
        }

        protected override System.Collections.IEnumerable GetViewModel(IEnumerable<EnterpriseRisk> results)
        {
            var enterpriseRisks = results.Select(enterpriseRisk => new EnterpriseRiskViewModel(enterpriseRisk)).ToArray();
            return enterpriseRisks;
        }
    }
}
