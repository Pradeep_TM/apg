﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class TestObjectiveManager : EntityManager<Entity.TestObjective>
    {
        
        public TestObjectiveManager() : base() { }
        public TestObjectiveManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public TestObjectiveManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
