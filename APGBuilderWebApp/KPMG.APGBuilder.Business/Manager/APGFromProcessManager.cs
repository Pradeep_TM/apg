﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class APGFromProcessManager : EntityManager<Entity.APG>
    {
        public APGFromProcessManager() : base() { }
        public APGFromProcessManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public APGFromProcessManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
