﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class SubProcessController : GridEntityController<SubProcess>
    {
        public SubProcessController() : base(new SubProcessManager()) { }

        public override ActionResult Index()
        {
            IList<SubProcess> businessProcessList = Manager.GetList();
            ViewBag.Controller = APGConstants.SUB_PROCESS;
            return View(GetViewModel(businessProcessList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }


        public override ActionResult Create()
        {
            return base.Create();
        }



        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {

            SubProcess bpObject = Manager.New();
            SubProcessViewModel viewModel = new SubProcessViewModel();
            TryUpdateModel(viewModel);

            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.CreatedBy = APGConstants.USER_NAME;
                bpObject.CreatedOn = DateTime.Now;
                Manager.Create(bpObject);
                viewModel.Id = bpObject.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);

        }

        public override ActionResult Edit(long id)
        {
            var viewModel = new SubProcessViewModel(Manager.Read(id));
            return View(viewModel);
        }

        public ActionResult ChangeActivation(long id, bool isActive)
        {
            SubProcess spObject = Manager.Read(id);
            spObject.ModifiedBy = Session[APGConstants.USER_NAME].ToString();
            spObject.ModifiedOn = DateTime.Now;
            spObject.IsActive = !isActive;
            Manager.Update(spObject);
            return RedirectToAction(APGConstants.INDEX);            
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            SubProcess bpObject = Manager.Read(id);
            SubProcessViewModel viewModel = new SubProcessViewModel();
            TryUpdateModel(viewModel);

            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.ModifiedBy = Session[APGConstants.USER_NAME].ToString();
                bpObject.ModifiedOn = DateTime.Now;
                Manager.Update(bpObject);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }


        public override ActionResult Delete(long id)
        {
            SubProcess bpObject = Manager.Read(id);
            bpObject.IsActive = false;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }


        #region Subprodess Industry Mapping
        private void CreateProjectForStandard(SubProcessViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {
                        dataContext.SubProcessIndustryMappings.InsertOnSubmit(new SubProcessIndustryMapping
                        {
                            SubProcessId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = "IN-PTHATHIGOWDAR2",
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        private void UpdateProjectForStandard(SubProcessViewModel model)
        {
            var subProcess = Manager.Read(model.Id);
            var existingIds = subProcess.SubProcessIndustryMappings.Select(x => x.SubIndustryId).Distinct().ToList();

            if (model.SelectedIndustry == null)
            {
                // if the industry mapping is not available
                model.SelectedIndustry = new List<long>().ToArray();
            }

            var deletedSubIndustryIds = existingIds.Except(model.SelectedIndustry).ToList();
            var addedSubIndustryIds = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                //Delete the subindustry mappings which were unselected
                if (deletedSubIndustryIds.Count > 0)
                {
                    var subIndustryMappings = dataContext.SubProcessIndustryMappings.Where(x => x.SubProcessId == model.Id && deletedSubIndustryIds.Contains(x.SubIndustryId))
                                                                                    .ToList();                    
                    dataContext.SubProcessIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                //Add new sub industries
                if (addedSubIndustryIds.Count > 0)
                {
                    addedSubIndustryIds.ForEach(selIndustry =>
                    dataContext.SubProcessIndustryMappings.InsertOnSubmit(new SubProcessIndustryMapping
                            {
                                SubProcessId = model.Id,
                                SubIndustryId = selIndustry,
                                CreatedBy = Convert.ToString(Session["User"]),
                                CreatedOn = DateTime.Now
                            })
                    );
                }

                dataContext.SubmitChanges();
            }
        }

        #endregion BusinessProcess Industry Mapping


        protected override object GetViewModel(SubProcess result)
        {
            return new SubProcessViewModel(result);
        }

        protected override bool TryUpdateViewModel(SubProcess result)
        {
            return TryUpdateModel(new SubProcessViewModel(result));
        }

        protected override IEnumerable GetViewModel(IEnumerable<SubProcess> results)
        {
            var subProcesses = results.Select(subProcess => new SubProcessViewModel(subProcess)).ToArray();
            return subProcesses;
        }
    }
}