USE [master]
GO
/****** Object:  Database [APGBuilder]    Script Date: 5/22/2015 3:03:20 PM ******/
CREATE DATABASE [APGBuilder]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'APGBuilder', FILENAME = N'C:\MyWorkItems\APGBuilder\Scripts\APGBuilder_14thApril' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'APGBuilder_log', FILENAME = N'C:\MyWorkItems\APGBuilder\APGBuilder_14thAprilLog' , SIZE = 3136KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [APGBuilder] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [APGBuilder].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [APGBuilder] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [APGBuilder] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [APGBuilder] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [APGBuilder] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [APGBuilder] SET ARITHABORT OFF 
GO
ALTER DATABASE [APGBuilder] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [APGBuilder] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [APGBuilder] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [APGBuilder] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [APGBuilder] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [APGBuilder] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [APGBuilder] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [APGBuilder] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [APGBuilder] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [APGBuilder] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [APGBuilder] SET  DISABLE_BROKER 
GO
ALTER DATABASE [APGBuilder] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [APGBuilder] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [APGBuilder] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [APGBuilder] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [APGBuilder] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [APGBuilder] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [APGBuilder] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [APGBuilder] SET RECOVERY FULL 
GO
ALTER DATABASE [APGBuilder] SET  MULTI_USER 
GO
ALTER DATABASE [APGBuilder] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [APGBuilder] SET DB_CHAINING OFF 
GO
ALTER DATABASE [APGBuilder] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [APGBuilder] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'APGBuilder', N'ON'
GO
USE [APGBuilder]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activity](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActivityIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActivityId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ActivityIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActivityLog]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Activity] [nvarchar](510) NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ActivityLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[APG]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APG](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[Reviewer] [nvarchar](510) NULL,
	[UpVote] [int] NULL,
	[DownVote] [int] NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_APG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[APGDetails]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APGDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[APGId] [bigint] NOT NULL,
	[StringLibraryId] [bigint] NOT NULL,
	[SelectFlag] [bit] NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_APGDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Audit]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Audit](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Screen] [nvarchar](510) NOT NULL,
	[Data] [xml] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BusinessProcess]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessProcess](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_BusinessProcess] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BusinessProcessIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessProcessIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessProcessId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_BusinessProcessIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BusinessRisk]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessRisk](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_BusinessRisk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BusinessRiskIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessRiskIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessRiskId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_BusinessRiskIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Control]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Control](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Control] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ControlIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControlIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ControlId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ControlIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Designation]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Designation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EnterpriseRisk]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnterpriseRisk](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_EnterpriseRisk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EnterpriseRiskIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnterpriseRiskIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EnterpriseRiskId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_EnterpriseRiskIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExecutionRisk]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionRisk](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ExecutionRisk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExecutionRiskIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecutionRiskIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ExecutionRiskId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ExecutionRiskIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Industry]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Industry](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[LOBId] [bigint] NOT NULL,
 CONSTRAINT [PK_Industry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LOB]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOB](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_LOB] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Process]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Process](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Process] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProcessIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcessIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcessId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ProcessIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleInUser]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleInUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CorpId] [nvarchar](255) NULL,
	[RoleId] [int] NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_RoleInUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StringLibrary]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringLibrary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessProcessId] [bigint] NOT NULL,
	[ProcessId] [bigint] NOT NULL,
	[SubProcessId] [bigint] NOT NULL,
	[ActivityId] [bigint] NOT NULL,
	[EnterpriseRiskId] [bigint] NOT NULL,
	[BusinessRiskId] [bigint] NOT NULL,
	[ExecutionRiskId] [bigint] NOT NULL,
	[ControlId] [bigint] NOT NULL,
	[TestObjectiveId] [bigint] NOT NULL,
	[TestId] [bigint] NOT NULL,
	[ReviewStatus] [bit] NULL,
	[ApproveStatus] [bit] NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_StringLibrary] PRIMARY KEY CLUSTERED 
(
	[BusinessProcessId] ASC,
	[ProcessId] ASC,
	[SubProcessId] ASC,
	[ActivityId] ASC,
	[EnterpriseRiskId] ASC,
	[BusinessRiskId] ASC,
	[ExecutionRiskId] ASC,
	[ControlId] ASC,
	[TestObjectiveId] ASC,
	[TestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubIndustry]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubIndustry](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IndustryId] [bigint] NOT NULL,
 CONSTRAINT [PK_SubIndustry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubProcess]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubProcess](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_SubProcess] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubProcessIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubProcessIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SubProcessId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_SubProcessIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Test]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[Hypothesis] [nvarchar](510) NULL,
	[Notes] [nvarchar](max) NULL,
	[Technique] [nvarchar](510) NULL,
	[System] [nvarchar](510) NULL,
	[SystemNotes] [nvarchar](max) NULL,
	[EstimatedHours] [numeric](10, 2) NULL,
	[Approved] [bit] NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TestIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TestId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TestIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TestObjective]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestObjective](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TestObjective] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TestObjectiveIndustryMapping]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestObjectiveIndustryMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TestObjectiveId] [bigint] NOT NULL,
	[SubIndustryId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TestObjectiveIndustryMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TestStep]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestStep](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TestId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[SortOrder] [int] NOT NULL,
	[Approved] [bit] NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TestStep] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[CorpId] [nvarchar](255) NOT NULL,
	[EmailId] [nvarchar](255) NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[CorpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoteAPG]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoteAPG](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[APGId] [bigint] NOT NULL,
	[VoteTypeId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_VoteAPG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoteTest]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoteTest](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TestId] [bigint] NOT NULL,
	[VoteTypeId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_VoteTest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoteType]    Script Date: 5/22/2015 3:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoteType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DesignationId] [bigint] NOT NULL,
	[Name] [nvarchar](510) NOT NULL,
	[Description] [nvarchar](510) NULL,
	[Score] [int] NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_VoteType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Activity] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ActivityIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ActivityLog] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[APG] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[APGDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Audit] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[BusinessProcess] ADD  CONSTRAINT [DF_BusinessProcess_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BusinessProcess] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[BusinessProcessIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[BusinessRisk] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[BusinessRiskIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Control] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ControlIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Designation] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EnterpriseRisk] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EnterpriseRiskIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ExecutionRisk] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ExecutionRiskIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Industry] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[LOB] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Process] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ProcessIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Role] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[StringLibrary] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SubIndustry] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SubProcess] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SubProcessIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Test] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TestIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TestObjective] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TestObjectiveIndustryMapping] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TestStep] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[VoteAPG] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[VoteTest] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[VoteType] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ActivityIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ActivityIndustryMapping_ActivityId] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[Activity] ([Id])
GO
ALTER TABLE [dbo].[ActivityIndustryMapping] CHECK CONSTRAINT [FK_ActivityIndustryMapping_ActivityId]
GO
ALTER TABLE [dbo].[ActivityIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ActivityIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[ActivityIndustryMapping] CHECK CONSTRAINT [FK_ActivityIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[APGDetails]  WITH CHECK ADD  CONSTRAINT [FK_APGDetails_APGId] FOREIGN KEY([APGId])
REFERENCES [dbo].[APG] ([Id])
GO
ALTER TABLE [dbo].[APGDetails] CHECK CONSTRAINT [FK_APGDetails_APGId]
GO
ALTER TABLE [dbo].[BusinessProcessIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_BusinessProcessIndustryMapping_BusinessProcessId] FOREIGN KEY([BusinessProcessId])
REFERENCES [dbo].[BusinessProcess] ([Id])
GO
ALTER TABLE [dbo].[BusinessProcessIndustryMapping] CHECK CONSTRAINT [FK_BusinessProcessIndustryMapping_BusinessProcessId]
GO
ALTER TABLE [dbo].[BusinessProcessIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_BusinessProcessIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[BusinessProcessIndustryMapping] CHECK CONSTRAINT [FK_BusinessProcessIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[BusinessRiskIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_BusinessRiskIndustryMapping_BusinessRiskId] FOREIGN KEY([BusinessRiskId])
REFERENCES [dbo].[BusinessRisk] ([Id])
GO
ALTER TABLE [dbo].[BusinessRiskIndustryMapping] CHECK CONSTRAINT [FK_BusinessRiskIndustryMapping_BusinessRiskId]
GO
ALTER TABLE [dbo].[BusinessRiskIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_BusinessRiskIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[BusinessRiskIndustryMapping] CHECK CONSTRAINT [FK_BusinessRiskIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[ControlIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ControlIndustryMapping_ControlId] FOREIGN KEY([ControlId])
REFERENCES [dbo].[Control] ([Id])
GO
ALTER TABLE [dbo].[ControlIndustryMapping] CHECK CONSTRAINT [FK_ControlIndustryMapping_ControlId]
GO
ALTER TABLE [dbo].[ControlIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ControlRiskIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[ControlIndustryMapping] CHECK CONSTRAINT [FK_ControlRiskIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[EnterpriseRiskIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_EnterpriseRiskIndustryMapping_EnterpriseRiskId] FOREIGN KEY([EnterpriseRiskId])
REFERENCES [dbo].[EnterpriseRisk] ([Id])
GO
ALTER TABLE [dbo].[EnterpriseRiskIndustryMapping] CHECK CONSTRAINT [FK_EnterpriseRiskIndustryMapping_EnterpriseRiskId]
GO
ALTER TABLE [dbo].[EnterpriseRiskIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_EnterpriseRiskIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[EnterpriseRiskIndustryMapping] CHECK CONSTRAINT [FK_EnterpriseRiskIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[ExecutionRiskIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionRiskIndustryMapping_ExecutionRiskId] FOREIGN KEY([ExecutionRiskId])
REFERENCES [dbo].[ExecutionRisk] ([Id])
GO
ALTER TABLE [dbo].[ExecutionRiskIndustryMapping] CHECK CONSTRAINT [FK_ExecutionRiskIndustryMapping_ExecutionRiskId]
GO
ALTER TABLE [dbo].[ExecutionRiskIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionRiskIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[ExecutionRiskIndustryMapping] CHECK CONSTRAINT [FK_ExecutionRiskIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[Industry]  WITH CHECK ADD  CONSTRAINT [FK_Industry_LOBId] FOREIGN KEY([LOBId])
REFERENCES [dbo].[LOB] ([Id])
GO
ALTER TABLE [dbo].[Industry] CHECK CONSTRAINT [FK_Industry_LOBId]
GO
ALTER TABLE [dbo].[ProcessIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ProcessIndustryMapping_ProcessId] FOREIGN KEY([ProcessId])
REFERENCES [dbo].[Process] ([Id])
GO
ALTER TABLE [dbo].[ProcessIndustryMapping] CHECK CONSTRAINT [FK_ProcessIndustryMapping_ProcessId]
GO
ALTER TABLE [dbo].[ProcessIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_ProcessIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[ProcessIndustryMapping] CHECK CONSTRAINT [FK_ProcessIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[RoleInUser]  WITH CHECK ADD  CONSTRAINT [FK_RoleInUser_CorpId] FOREIGN KEY([CorpId])
REFERENCES [dbo].[User] ([CorpId])
GO
ALTER TABLE [dbo].[RoleInUser] CHECK CONSTRAINT [FK_RoleInUser_CorpId]
GO
ALTER TABLE [dbo].[RoleInUser]  WITH CHECK ADD  CONSTRAINT [FK_RoleInUser_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[RoleInUser] CHECK CONSTRAINT [FK_RoleInUser_RoleId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_ActivityId] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[Activity] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_ActivityId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_BusinessProcessId] FOREIGN KEY([BusinessProcessId])
REFERENCES [dbo].[BusinessProcess] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_BusinessProcessId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_BusinessRiskId] FOREIGN KEY([BusinessRiskId])
REFERENCES [dbo].[BusinessRisk] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_BusinessRiskId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_ControlId] FOREIGN KEY([ControlId])
REFERENCES [dbo].[Control] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_ControlId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_EnterpriseRiskId] FOREIGN KEY([EnterpriseRiskId])
REFERENCES [dbo].[EnterpriseRisk] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_EnterpriseRiskId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_ExecutionRiskId] FOREIGN KEY([ExecutionRiskId])
REFERENCES [dbo].[ExecutionRisk] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_ExecutionRiskId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_ProcessId] FOREIGN KEY([ProcessId])
REFERENCES [dbo].[Process] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_ProcessId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_SubProcessId] FOREIGN KEY([SubProcessId])
REFERENCES [dbo].[SubProcess] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_SubProcessId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_TestId] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_TestId]
GO
ALTER TABLE [dbo].[StringLibrary]  WITH CHECK ADD  CONSTRAINT [FK_StringLibrary_TestObjectiveId] FOREIGN KEY([TestObjectiveId])
REFERENCES [dbo].[TestObjective] ([Id])
GO
ALTER TABLE [dbo].[StringLibrary] CHECK CONSTRAINT [FK_StringLibrary_TestObjectiveId]
GO
ALTER TABLE [dbo].[SubIndustry]  WITH CHECK ADD  CONSTRAINT [FK_SubIndustry_IndustryId] FOREIGN KEY([IndustryId])
REFERENCES [dbo].[Industry] ([Id])
GO
ALTER TABLE [dbo].[SubIndustry] CHECK CONSTRAINT [FK_SubIndustry_IndustryId]
GO
ALTER TABLE [dbo].[SubProcessIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_SubProcessIndustryMapping_ProcessId] FOREIGN KEY([SubProcessId])
REFERENCES [dbo].[SubProcess] ([Id])
GO
ALTER TABLE [dbo].[SubProcessIndustryMapping] CHECK CONSTRAINT [FK_SubProcessIndustryMapping_ProcessId]
GO
ALTER TABLE [dbo].[SubProcessIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_SubProcessIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[SubProcessIndustryMapping] CHECK CONSTRAINT [FK_SubProcessIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[TestIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_TestIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[TestIndustryMapping] CHECK CONSTRAINT [FK_TestIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[TestIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_TestIndustryMapping_TestId] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([Id])
GO
ALTER TABLE [dbo].[TestIndustryMapping] CHECK CONSTRAINT [FK_TestIndustryMapping_TestId]
GO
ALTER TABLE [dbo].[TestObjectiveIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_TestObjectiveIndustryMapping_SubIndustryId] FOREIGN KEY([SubIndustryId])
REFERENCES [dbo].[SubIndustry] ([Id])
GO
ALTER TABLE [dbo].[TestObjectiveIndustryMapping] CHECK CONSTRAINT [FK_TestObjectiveIndustryMapping_SubIndustryId]
GO
ALTER TABLE [dbo].[TestObjectiveIndustryMapping]  WITH CHECK ADD  CONSTRAINT [FK_TestObjectiveIndustryMapping_TestObjectiveId] FOREIGN KEY([TestObjectiveId])
REFERENCES [dbo].[TestObjective] ([Id])
GO
ALTER TABLE [dbo].[TestObjectiveIndustryMapping] CHECK CONSTRAINT [FK_TestObjectiveIndustryMapping_TestObjectiveId]
GO
ALTER TABLE [dbo].[TestStep]  WITH CHECK ADD  CONSTRAINT [FK_TestStep_TestId] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([Id])
GO
ALTER TABLE [dbo].[TestStep] CHECK CONSTRAINT [FK_TestStep_TestId]
GO
ALTER TABLE [dbo].[VoteAPG]  WITH CHECK ADD  CONSTRAINT [FK_VoteAPG_APGId] FOREIGN KEY([APGId])
REFERENCES [dbo].[APG] ([Id])
GO
ALTER TABLE [dbo].[VoteAPG] CHECK CONSTRAINT [FK_VoteAPG_APGId]
GO
ALTER TABLE [dbo].[VoteAPG]  WITH CHECK ADD  CONSTRAINT [FK_VoteAPG_VoteTypeId] FOREIGN KEY([VoteTypeId])
REFERENCES [dbo].[VoteType] ([Id])
GO
ALTER TABLE [dbo].[VoteAPG] CHECK CONSTRAINT [FK_VoteAPG_VoteTypeId]
GO
ALTER TABLE [dbo].[VoteTest]  WITH CHECK ADD  CONSTRAINT [FK_VoteTest_TestId] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([Id])
GO
ALTER TABLE [dbo].[VoteTest] CHECK CONSTRAINT [FK_VoteTest_TestId]
GO
ALTER TABLE [dbo].[VoteTest]  WITH CHECK ADD  CONSTRAINT [FK_VoteTest_VoteTypeId] FOREIGN KEY([VoteTypeId])
REFERENCES [dbo].[VoteType] ([Id])
GO
ALTER TABLE [dbo].[VoteTest] CHECK CONSTRAINT [FK_VoteTest_VoteTypeId]
GO
ALTER TABLE [dbo].[VoteType]  WITH CHECK ADD  CONSTRAINT [FK_VoteType_DesignationId] FOREIGN KEY([DesignationId])
REFERENCES [dbo].[Designation] ([Id])
GO
ALTER TABLE [dbo].[VoteType] CHECK CONSTRAINT [FK_VoteType_DesignationId]
GO
USE [master]
GO
ALTER DATABASE [APGBuilder] SET  READ_WRITE 
GO
