﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace KPMG.APGBuilder.Web.Models
{
    public class ExecutionRiskViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public ExecutionRiskViewModel() { }
        public ExecutionRiskViewModel(ExecutionRisk executionRisk)
        {
            if (executionRisk != null)
            {
                this.Id = executionRisk.Id;
                this.Name = executionRisk.Name;
                this.Description = executionRisk.Description;
                this.IsActive = executionRisk.IsActive;
                if (executionRisk.ExecutionRiskIndustryMappings != null && executionRisk.ExecutionRiskIndustryMappings.Count > 0)
                {
                    this.SelectedIndustry = executionRisk.ExecutionRiskIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    this.SubIndustry = string.Join(", ", executionRisk.ExecutionRiskIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }
            this.executionRisk = executionRisk;
        }

        public ExecutionRisk executionRisk { get; set; }

        public long Id { get; set; }
        [Required(ErrorMessage = "Execution Risk Name is Required...")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Execution Risk Description is Required...")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Select Execution Risk Status..."),DisplayName("Status")]
        public bool? IsActive { get; set; }

        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        [Required(ErrorMessage = "Select one Industry")]
        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long subIndustryId, long executionRiskId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.ExecutionRiskIndustryMappings
                              where industryMapping.SubIndustryId == subIndustryId
                              && industryMapping.ExecutionRiskId == executionRiskId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping


    }
}