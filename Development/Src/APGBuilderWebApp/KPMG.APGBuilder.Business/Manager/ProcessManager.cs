﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class ProcessManager : EntityManager<Entity.Process>
    {
        public ProcessManager() : base() { }
        public ProcessManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public ProcessManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
