﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class TestManager : EntityManager<Entity.Test>
    {
        public TestManager() : base() { }
        public TestManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public TestManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
