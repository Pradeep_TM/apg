﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class ActivityManager : EntityManager<Entity.Activity>
    {
        public ActivityManager() : base() { }
        public ActivityManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public ActivityManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
