﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KGS.Framework.Business;
using KGS.Framework.Web.Mvc.Telerik;
using KPMG.APGBuilder.Web.Models;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class TestController : GridEntityController<Test>
    {
        //
        // GET: /Test/

        public TestController() : base(new TestManager()) { }

        //public ActionResult Index()
        //{
        //    TestViewModel viewModel = new TestViewModel();
        //    return View(viewModel);
        //}

        public override ActionResult Create()
        {
            TestViewModel viewModel = new TestViewModel();
            return View(viewModel);
        }


        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            Test testObject = Manager.New();
            TestViewModel viewModel = new TestViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                testObject.Name = viewModel.Name;
                testObject.Description = viewModel.Description;
                testObject.Hypothesis = viewModel.Hypothesis;
                testObject.Notes = viewModel.Notes;
                testObject.Technique = viewModel.Technique;
                testObject.System = viewModel.System;
                testObject.SystemNotes = viewModel.SystemNotes;
                testObject.EstimatedHours = (decimal)viewModel.EstimatedHours;
                testObject.Approved = viewModel.Approved;
                testObject.IsActive = viewModel.IsActive;
                testObject.CreatedBy = Session["UserName"].ToString();
                testObject.CreatedOn = DateTime.Now;
                Manager.Create(testObject);
                viewModel.Id = testObject.Id;
                //CreateProjectForStandard(viewModel);
                return RedirectToAction("Create");
            }
            return View(viewModel);
        }


    }
}
