/******************************************************
** lib.js version 1.46 August 25, 2011
*******************************************************/

/* search item constructor */
function _SearchItem(strName,strUrl,strIsDefault,strAuth){
	this.name = strName;
	this.url = strUrl;
	this.isdefault = strIsDefault;
	this.auth = strAuth;
}
/* menu item constructor */
function _Item(text, link, nw, nwProp) {
   this.text=text; 
   this.nw=nw+""; 
   this.nwProp=nwProp;
   if(link=="") {
		this.link="#";
   } else {
		this.link=link;
   }
	
	/* Identify from the site_data settin of this is an off-site link */
	var varString=this.nw.split(",");
	var linkType="";
			
	for(j=0;varString[j]!=null;j++) {
	 	if(varString[j].indexOf('linktype')!=-1) {
			linkType=varString[j].substring(varString[j].indexOf('=')+1,varString[j].length).replace(/ /g, "");
		}
	}
			
	if(linkType=="off-site") {
		this.text+="<img src="+_page.commonServer+"/img/offsitelinktop.gif border=0>";
	}
}
/* site constructor */
function _Site(text, link) {
   this.link=link;
   this.text=text;
}
/* button constructor */
function _Button(text, img, link,width,target) {
   this.link=link;
   this.img=img;
   this.text=text;
   this.width=width;
   this.target=target;
}
/* page constructor */
function _Page() {
	this.pageScript = function(){};
	this.callcount = 0;
	this.searchItems = new Array()		 /* array of search items */
	this.searchItemsAuth = new Array();	/* array of authenticated searches */
	this.items = new Array()             /* array of menu items  */
	this.sites = new Array()             /* array of sites       */
	this.siteIdentifier = "";            /* the Site Identifier  */
	this.item  = "0";                    /* the current selected menu item, default: home */
	this.hideLeftNavigation = false;     /* parameter to control display of left navigation bar, default: false */
	this.trace = false;                  /* if true a _popup window will appear showing generated code */
	this.ignoreFramed = false; 			 /* if true, the menu will be displayed normally, even if started from within iframe*/
	this.traceWindow = "0";
	this.traceVar = '';
	this.commonServer;
	this.commonTools;
	this.buttons = new Array();
	this.listSM=new Array();	
	this.listNr=0;
	this.ns = (navigator.appName=='Netscape'?1:0);
	if (navigator.userAgent.indexOf('Netscape/7')>-1) {this.ns7='1';}
	this.topItem='border-width:0px 0px 1px 0px;';
	this.nsHand=(this.ns==1?'cursor:pointer;':'');
	this.noLink='cursor:default;';
	this.pageready = false;
	this.loadTools = true;
	this.delayCollapse=1000;
	this.delayExpand=300;	
	this.menuactive="no";
	this.comresType="www";
	var p=this;
		
	/* Check if the comres is called on internet or intranet */
	scriptArray=document.getElementsByTagName("script");
	for(i=0;i<scriptArray.length;i++){
		scriptSrc=scriptArray[i].src;
		if(scriptSrc.indexOf('/lib.js')!=-1) {
			p.commonServer=scriptSrc.substring(0,scriptSrc.indexOf('/lib.js'));

		}
	}

		
	p.lhimgoff = new Image(1,35);
	p.lhimgon = new Image(1,35);
	p.lhimgover = new Image(1,35);

	
}
_Page.prototype.img = function(w,h,a,c,i) {
	if (h==null) h=1;
	if (w==null) w=1;
	if (a==null) a='';
	if (c==null) c='';
	if (i==null) i='t.gif';
	var html='<img src="'+this.commonServer+'/img/'+i+'"'+(c==''?'':' class="'+c+'"')+' width="'+w+'" height="'+h+'" alt="'+a+'" border="0" />';
	return html;
}
_Page.prototype.showLeftNavigator = function() {
    /* determine if we need to show left navigator */
    return(!(this.item==null ||
             this.item==""   ||
             this.hideLeftNavigation))
}
_Page.prototype.output = function(html) {
	var encoded_html;
	if (this.trace) {
		encoded_html=html.replace(/\&/g,"&amp;");
		encoded_html=encoded_html.replace(/>/g,"&gt;");
		encoded_html=encoded_html.replace(/</g,"&lt;");
		encoded_html=encoded_html.replace(/\n/g,"<br />");
		this.traceWindow.document.write(encoded_html)
	}
	document.write(html)
}
_Page.prototype.input = function(html) {
	html=html.replace("&gt;",">");
	html=html.replace("&lt;","<");
	html=html.replace("&gt;",">");
	html=html.replace("&lt;","<");
	return(html)
}

// trim function
_Page.prototype.trim = function(value) {
	value = value.replace(/^\s+/,'');
	value = value.replace(/\s+$/,'');
	return value;
}
//OnResize window
onHeaderResize = function() {
	if(document.getElementById("p-headersearch") != null) {
		_tmpX = _page.findPosX(document.getElementById("p-headersearch"));
		document.getElementById("p-authscopediv").style.top = 3;
		document.getElementById("p-authscopediv").style.left = _tmpX;
	}
}

_Page.prototype.onHeaderSearchFocus = function(event){
	if(document.forms[0] != null){
		if(document.forms[0].elements[0] != null){
			if(document.forms[0].elements[0].id == "p-headersearch"){
				//set action in parent form to search.
				this._tmpFormAction = document.forms[0].action;
				document.forms[0].action = "javascript:_page.postHeaderSearch();";
			}
		}
	}
}
_Page.prototype.onHeaderSearchBlur = function(event){
	if(document.forms[0] != null){
		if(document.forms[0].elements[0] != null){
			if(document.forms[0].elements[0].id == "p-headersearch"){
				//set action in parent form to default.
				document.forms[0].action = this._tmpFormAction;
			}
		}
	}
}
_Page.prototype.onHeaderChange = function(){
	tmpScope = _page.getSelectedScope();
	for(i=0;i<_page.searchItemsAuth.length;i++){
		document.getElementById("p-authscopediv").style.display = 'none';
		if(_page.searchItemsAuth[i] == tmpScope){
			_tmpX = _page.findPosX(document.getElementById("p-headersearch"));
			document.getElementById("p-authscopediv").style.top = 3;
			document.getElementById("p-authscopediv").style.left = _tmpX;
			document.getElementById("p-authscopediv").style.display = '';
			return;
		}
	}
}
_Page.prototype.getSelectedScope = function() {
		var arrValues = document.getElementById("p-headersearch");
		var strScope = "";
		for(var i = 0; i < arrValues.length; i++) {
			if (arrValues[i].selected == true) {
				strScope = arrValues[i].value;
				return strScope
			}
		}
}
_Page.prototype.showPopup = function(url,winName,W,H,X,Y) {
	if(W == undefined) W = 400;	//default width
	if(H == undefined) H = 300; //default height
	if(X == undefined) X = screen.width/2-W/2; //default Xpos (is now centered)
	if(Y == undefined) Y = screen.height/2-H/2; //default Ypos (is now centered)
	winprops = 'height='+H+',width='+W+',top='+X+',left='+Y+',scrollbars=no';
	win = window.open(url,winName,winprops);
	if (parseInt(navigator.appVersion) >= 4) {
		win.window.focus();
	}
}
_Page.prototype.findPosX = function(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }
 window.onresize = onHeaderResize;
  
_Page.prototype.showButtons  = function() {

	var search_html='';
	search_html+='<td class="p-globallinks">'+this.img(18,1)+'search:'+this.img(5,1)+'</td>';
	search_html+='<td><select id="p-headersearch" onchange="_page.onHeaderChange();"></select>'+this.img(5,1,null,null,'t.gif')+'</td>';
	search_html+='<td><input type="text" id="p-headerquery" onblur="_page.onHeaderSearchBlur(event);" onfocus="_page.onHeaderSearchFocus(event);" onkeyup="_page.onHeaderSearchKeyPost(event);"/></td>';
	search_html+='<td><a href="javascript:_page.postHeaderSearch()" style="margin-bottom:50px;">'+this.img(5,1,null,null,'t.gif')+this.img(26,24,null,null,'but_gooff.gif')+'</a>';
	search_html+='<script src="'+this._tmpProtocol+this._tmpSearchActionUrl+this._tmpSearchPath+'HeaderSearch.aspx?domain='+document.domain+'&appcode='+cssappcode+'"></script>';
	search_html+='<script src="'+this._tmpProtocol+this._tmpSearchActionUrl+this._tmpSearchPath+'SetCookie.aspx"></script>';
	search_html+='</td>';
	
	return(buttons_html);
}

_Page.prototype.startPage = function (item) {
    var p = this;
    /* Calls the statistics logger */
    if (window.location.protocol == "http:") {
        //Prevents metrics tag to be included on HTTPS sites.
        p.statCall();
    }

    /* figure out the window width */
    function getIEVersion() {
        var ua = navigator.userAgent;
        var IEOffset = ua.indexOf('MSIE ')
        return parseFloat(ua.substring(IEOffset + 5, ua.indexOf(';', IEOffset)))
    }

    function linkLookup(p, l) {
        for (i in p.items) {
            if (l == p.items[i].link) {
                return (i);
            }
        }
        return "";
    }

    if (p.ns == '1') {
        p.clientWidth = window.innerWidth - 16;
        p.clientHeight = window.innerHeight - 14;
    } else {
        if (document.compatMode && document.compatMode != 'BackCompat') {
            p.clientWidth = document.documentElement.clientWidth;
            p.clientHeight = document.documentElement.clientHeight - 20;
        } else {
            p.clientWidth = document.body.clientWidth - ((getIEVersion() >= 5 && getIEVersion() < 5.5) ? 18 : 0);
            p.clientHeight = document.body.clientHeight - 16;
        }
    }

    /* Check if the page is framed */
    try {
        if (self != top && !p.ignoreFramed) {
            p.framed = "1";
        } else {
            p.framed = "0";
        }
    }
    catch (e) {
        p.framed = "1";
       
    }

    if (p.framed != "1") {
        //BEGIN: Check if this page is framed.

        /* If no item is passed as parameter, try and lookup the current location */
        if (item == null) {
            var tmp = linkLookup(p, document.location.pathname);
            p.item = (tmp == "") ? "-1" : tmp;
        } else {
            if (item == "") {
                p.item = "-1";
            } else {
                p.item = item;
            }
        }
        if ((p.item == "-1" || p.item == "0") && !p.hideLeftNavigation) p.hideLeftNavigation = true;
        /* Global variables */
        var hierArr = p.item.split("_");
        var currentLevel = hierArr.length;
        var deepest = (p.items[p.item + "_1"] == null) ? true : false;
        var comsvr = p.commonServer;
        var currentItem = p.items[p.item];
        p.traceVar += 'current level=' + currentLevel + '<br>deepest=' + deepest;

        function leftNavigation(p) {
            var html = '';
            function boldItem(t, l, a) {
                if (t != undefined) {
                    if (t.nw == true) {
                        var strTarget = "_blank";
                    } else if (t.nw.replace(/ /g, "").indexOf('newwindow=yes') != -1) {
                        var strTarget = "_blank";
                    } else {
                        var strTarget = ""; ;
                    }
                }
                return '\
	                <tr id="p-lntrpar">\n\
		                <td id="p-lnentry">\n\
			                <table border="0" cellspacing="0" cellpadding="0">\n\
				                <tr valign="top">\n\
					                <td class="p-lnnavb' + l + '">' + p.img(16, 15, null, null, 'ln_arrow' + ((a) ? '_exp' : '') + '.gif') + '</td>\n\
					                <td class="p-lnnavtb"><a href="' + t.link + '"' + (strTarget != '' ? 'target="' + strTarget + '"' : '') + '><b>' + t.text + '</b></a></td>\n\
				                </tr>\n\
			                </table>\n\
		                </td>\n\
	                </tr>\n';
            }

            function boldItemCur(t, l) {
                if (t.nw == true) {
                    var strTarget = "_blank";
                } else if (t.nw.replace(/ /g, "").indexOf('newwindow=yes') != -1) {
                    var strTarget = "_blank";
                } else {
                    var strTarget = ""; ;
                }
                var s = '\
	<tr id="p-lntrsel">\n\
		<td id="p-lnentry">\n\
			<table border="0" cellspacing="0" cellpadding="0">\n\
				<tr valign="top">\n\
					<td class="p-lnnavb' + l + '">';
                if (!(l == 1 && deepest)) {
                    s += p.img(16, 15, null, null, 'ln_arrow_exp.gif');
                } else {
                    s += p.img(16, 15);
                }
                s += '</td>\n\
					<td class="p-lnnavtb"><a href="' + t.link + '"' + (strTarget != '' ? 'target="' + strTarget + '"' : '') + '><b>' + t.text + '</b></a></td>\n\
				</tr>\n\
			</table>\n\
		</td>\n\
	</tr>\n'
                return s;
            }
            function normItem(t, l, i) {
                if (t.nw == true) {
                    var strTarget = "_blank";
                } else if (t.nw.replace(/ /g, "").indexOf('newwindow=yes') != -1) {
                    var strTarget = "_blank";
                } else {
                    var strTarget = ""; ;
                }
                var s = '\
	<tr class="p-lnitem">\n\
		<td id="p-lnentry">\n\
			<table border="0" cellspacing="0" cellpadding="0">\n\
				<tr valign="top">\n\
					<td class="p-lnnavl' + l + '">';
                if (!(p.items[i + "_1"] == null)) {
                    s += p.img(16, 13, null, null, 'ln_arrow_right.gif');
                } else {
                    s += p.img(16, 13);
                }
                s += '</td>\n\
					<td class="p-lnnavtn"><a href="' + t.link + '"' + (strTarget != '' ? 'target="' + strTarget + '"' : '') + '>' + t.text + '</a></td>\n\
				</tr>\n\
			</table>\n\
		</td>\n\
	</tr>\n'
                return s;
            }
            function normItemCur(t, l, a) {
                if (t.nw == true) {
                    var strTarget = "_blank";
                } else if (t.nw.replace(/ /g, "").indexOf('newwindow=yes') != -1) {
                    var strTarget = "_blank";
                } else {
                    var strTarget = ""; ;
                }
                return '\
	<tr class="p-lnitem" id="p-lntrsel">\n\
		<td style="' + ((a) ? 'border-top:1px solid #B5B7C0; ' : '') + 'border-bottom:1px solid #FFFFFF;">\n\
			<table border="0" cellspacing="0" cellpadding="0"><tr valign="top"><td class="p-lnnavl' + l + '">\n' + p.img(16, 15) + '</td><td class="p-lnnavtn"><a href="' + t.link + '"' + (strTarget != '' ? 'target="' + strTarget + '"' : '') + '>' + t.text + '</a></td></tr></table>\n\
		</td>\n\
	</tr>\n';
            }
            function showParents() {
                var s = '', x = '', m;
                for (var i = 0; i < hierArr.length - 1; i++) {
                    x += hierArr[i];
                    m = p.items[x];
                    if ((i == hierArr.length - 2) && deepest && currentLevel > 1) {
                        s += boldItem(m, i + 1, true)
                    } else {
                        s += boldItem(m, i + 1, false)
                    }
                    x += '_';
                }
                return s;
            }
            function showChildren(x) {
                var s = '', c = '', m, l = currentLevel + 1;
                if (deepest && currentLevel > 1) l = currentLevel;
                for (var i = 1; !(p.items[x + "_" + i] == null); i++) {
                    c = x + "_" + i;
                    m = p.items[c];
                    if (c == p.item) {
                        s += normItemCur(m, l, (i == 1) ? false : true);
                    } else {
                        s += normItem(m, l, c);
                    }
                }
                return s;
            }
            function showSiblings() {
                var i = hierArr.slice(0, -1).join("_");
                var s = showChildren(i);
                return s;
            }
            html += '\
<table width="190" height="10" border="0" cellspacing="0" cellpadding="0" id="p-lntable">\n\
	<tr>\n\
		<td style="height:20px;font-size:1px;border-bottom:1px solid #b5b7b0">' + p.img(1, 20) + '</td>\n\
	</tr>\n';
            html += showParents();
            if (deepest && currentLevel > 1) {
                html += showSiblings();
            } else {
                html += boldItemCur(currentItem, currentLevel);
                html += showChildren(p.item);
            }
            html += '\
		<tr><td>&nbsp;</td></tr>\n\
</table>\n';
            p.output('<div id="p-leftnavigation">' + html + '</div>');
            return (html);
        }
        function checkRSStyle(i, p) {
            var s, l = p.sites[i].link;
            style = (i == p.sites.length - 1 ? 'padding-bottom:7px;' : '');

            if (l == '' | l == '#' | l == 'javascript:void(null)') {
                s += p.noLink;
            } else {
                s += p.nsHand;
            }
            return s == '' ? '' : ' style="' + s + '"';
        }
        function relatedSitesLayer(p) {
            var s = '\
<table id="p-siteident" cellspacing="0" cellpadding="0" style="position: absolute; top:0px; left: 0px; visibility:hidden"><tr><td class="p-siteidentifier" nowrap="nowrap">\n\
	' + p.siteIdentifier + '\n\
</td></tr></table>\n';
            if (_page.ns != '1') {
                s += '<IFRAME frameborder="0" src="' + _page.commonServer + '/img/t.gif" id=if-p-rsdiv scroll=none style="FILTER:progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0);visibility:hidden;height:0;position:absolute;width:0px;top:0px;z-index:2"></iframe>'
            }
            s += '<div id="p-rsdiv">\n\
	<table id="p-relsitelay" cellspacing="0" cellpadding="0" border="0"><tr><td>\n\
		<table id="p-rstwh" cellspacing="0" cellpadding="0">\n';
            for (var i = 0; i < p.sites.length; i++) {
                var m = p.sites[i];
                s += '\
					<tr><td' + (i == 0 ? ' nowrap="nowrap"' : '') +
					' id="p-rsrow_' + i + '" class="p-rstd"' +
					checkRSStyle(i, p) +
					' onmouseover="_MOverRS(this)" onmouseout="_MOutRS(this)" onclick="_MClickRS(this)">' + m.text + '</td></tr>\n';
            }
            s += '\
				</table>\n\
	</td>\n\
	</table>\n\
</div>\n';
            return s;
        }
        function relatedSitesLink(p) {
            var s = '\
<table id="p-rs-table" width="180" border="0" cellspacing="0" cellpadding="0">\n\
	<tr id="p-rs-row2">\n';
            if (p.sites.length == 0) {
                s += '<td id="p-relatedlayerplace" align="left" nowrap="nowrap">&nbsp;</td>';
            } else {
                if (p.sites.length == 1) {
                    s += '<td align="right" style="padding-top:5px;" nowrap="nowrap"><a href="' + p.sites[0].link + '">' + p.sites[0].text + '</a></td>';
                } else {
                    s += '<td class="p-rs-border"><table cellpadding="0" cellspacing="0" border="0" width="180"><tr><td id="p-relatedlayerplace" align="left" nowrap="nowrap">&nbsp;Related sites</td><td width="22" onClick="_showHideRelatedSites()" onmouseout="_MCloseRS(true)">' + p.img(22, 22, null, 'p-syst', 'rs_arrow.gif') + '</td></tr></table></td>\n'
                }
            }
            s += '\
	</tr>\n\
</table>\n'
            return s;
        }
        function checkHome(i, l, p) {
            var s;
            s = (i == 0 ? 'padding-left:0px;' : '');
            if (l == '' | l == '#' | l == 'javascript:void(null)') {
                s += p.noLink;
            } else {
                s += p.nsHand;
            }
            return s == '' ? '' : ' style="' + s + '"';
        }
        function topBar(p) {
            var x, s = '', i;
            var u = p.item.indexOf('_');
            var inx = u < 1 ? p.item : p.item.slice(0, u);
            s += '<table cellpadding="0" cellspacing="0" border="0">\n';
            s += '<tr>\n';

            for (i = 0; p.items[i.toString()] != null; i++) {
                var x = i.toString();
                s += '<td id="p-mi_' + i + '" class="p-ti' + (x == inx ? 'h' : '') + '"' + checkHome(i, p.items[x].link, p) + ' onmouseover="_MExpand(this)" onmouseout="_MOut(this)" onclick="_MClick(this)" nowrap="nowrap">';
                s += p.items[x].text;
                s += '</td>\n';
            }
            s += '</tr>\n';
            s += '</table>\n';
            p.output('<div id="p-topbar">\n' + s + '</div>\n');
            return s;
        }
        if (p.trace) {
            p.traceWindow = window.open()
            p.traceWindow.document.open()
            p.traceWindow.document.write('<html><body><h1>Trace of generated HTML</h1><pre>')
        }
        p.output(relatedSitesLayer(p));
        var rsl = document.getElementById('p-relsitelay').offsetWidth;
        rsl = rsl < 180 ? 180 : rsl;
        var tb = topBar(p), ln, lnw;
        if (p.showLeftNavigator()) {
            ln = leftNavigation(p);
            lnw = 160;
        } else {
            lnw = 220;
        }
        var tbw = document.getElementById('p-topbar').offsetWidth;
        tbw = tbw < (p.clientWidth - 54) ? p.clientWidth - 54 : tbw;
        document.getElementById('p-topbar').innerHTML = '&nbsp;';

        var html = '\
<table id="p-body" border="0" cellspacing="0" cellpadding="0" width="100%"> \n\
	<tr id="p-row1" class="p-bgffffff"> \n\
		<td width="10">' + p.img(10, 1) + '</td>\n\
		<td width="15">' + p.img(15, 1) + '</td>\n\
		<td>' + p.img(lnw, 1) + '</td>\n\
		<td width="15">' + p.img(15, 1) + '</td>\n\
		<td>' + p.img(tbw - lnw - 16, 1) + '</td>\n\
		<td width="15">' + p.img(15, 1) + '</td>\n\
		<td width="10">' + p.img(10, 1) + '</td>\n\
	</tr>\n\
	<tr id="p-row2" class="p-bgffffff">\n\
    <td colspan="2">&nbsp;</td>\n\
		<td colspan="3">\n\
			<table border="0" cellspacing="0" cellpadding="0">\n\
				<tr>\n\
					<td>' + p.img(114, 21, 'KPMG Logo', null, 'KPMG-logo.png') + '</td><td width="100%">&nbsp;</td><td style="text-alight:right">&nbsp;</td>\n\
					' + p.showButtons() + '\
				</tr>\n\
			</table>\n\
		</td>\
		<td colspan="2">&nbsp;</td>\n\
	</tr>\n\
	<tr id="p-row3" class="p-bgffffff">\n\
		<td colspan="7">&nbsp;</td>\n\
	</tr>\n\
	<tr id="p-row4">\n\
		<td id="p-ontop" colspan="7">\n\
			<table border="0" cellspacing="0" cellpadding="0">\n\
				<tr>\n\
					<td>' + p.img(24, 1, null, null, null) + '</td>\n\
					<td class="p-siteidentifier" nowrap="nowrap">' + p.siteIdentifier + '\n\
                                        </td>\n' +
					(p.sites.length == 0 ? '' : '\
					<td>' + p.img(p.clientWidth - rsl - document.getElementById('p-siteident').offsetWidth - 50, 1) + '</td>\n\
					<td valign="top">\n' + relatedSitesLink(p) + '\n\
					</td>\n') + '\
				</tr>\n\
			</table>\n\
		</td>\n\
	</tr>\n\
	<tr id="p-row5" valign="bottom">\n';
        if (p.item == "0") {
            html += '\
					<td colspan="2" width="15" id="p-snlefthome" onclick="_MClickLH(this)"' + (p.ns == 1 ? 'style="cursor:pointer;"' : 'style="cursor:hand;"') + '>' + p.img(15, 1) + '</td>\n';
        } else {
            html += '\
					<td colspan="2" width="15" id="p-snleft" onmouseover="_MOverLH(this)" onmouseout="_MOutLH(this)" onclick="_MClickLH(this)"' + (p.ns == 1 ? 'style="cursor:pointer;' : 'style="cursor:hand;') + 'background-image:url(' + comsvr + '/img/sn_bg.gif);">' + p.img(15, 1) + '</td>\n';
        }
        html += '<td id="p-snmiddle" colspan="3">' + tb + '</td>\n\
		<td id="p-snright" colspan="2">&nbsp;</td>\n\
	</tr>\n'
        if (p.showLeftNavigator()) {
            /* show left navigator */
            html += '\
	<style>body{background: url(' + _page.commonServer + '/img/ln_page_bg.gif);background-repeat: repeat-y;s}</style>\n\
	<tr id="p-row6" valign="top">\n\
		<td colspan="3" class="p-bge4e6ef" width="165" height="100%">\n\
				<!-- begin left navigation -->\n';
            html += ln;
            html += '\
		<!-- end left navigation -->\n\
		</td>\n\
		<td class="p-bgffffff" width="15">&nbsp;</td>\n\
		<td class="p-bgffffff">\n\
		<!-- start of content area -->\n\n'
        }
        else {
            /* hide left navigator */
            html += '\
	<tr id="p-row6" valign="top">\n\
		<td id="p-left" height="100%">&nbsp;</td>\n\
		<td id="p-leftinside">&nbsp;</td>\n\
		<td colspan="3" class="p-bgffffff" height="100%">\n\
		<!-- start of content area -->\n\n'
        }

        //END: Check if this page is framed. 
    } else {
        //FRAMED
        document.body.style.padding = '10px';
        html = '<table border="0" cellspacing="0" cellpadding="0" width="100%"> \n\
		<tr class="p-bgffffff"><td width="100%">';
    }

    p.output(html);
}
_Page.prototype.siteMap = function() {
	var p=this;
	var html='\
<table border="0" cellspacing="0" cellpadding="0" class="p-sitemap" width="100%">\n\
	<tr>\n';
	function doTable(p, i, l){
		var t;
		var html='\
				<tr>\n\
					<td class="p-sitemapr'+(l>1?2:l)+'"'+(l>1?' style="padding-left:'+(l*16-20)+'px"':'')+'>\n';
		if (l>1) html+='\
					<ul><li>\n';
		html+='\
						<a href="'+p.items[i].link+'">'+p.items[i
].text+'</a></td>\n';
		if (l>1) html+='\
					</li></ul>\n';
		html+='\
				</tr>\n';
		p.output(html);
		if (p.items[i+'_1']!=null) {
			for(t=1; p.items[i+'_'+t.toString()]!=null; t++) {
				doTable(p, i+"_"+t.toString(), l+1);
			}
		}
	}
	for(var i=1; p.items[i.toString()]!=null; i++) {
		html+='\
		<td valign="top" width="33%">\n\
			<table border="0" cellspacing="0" cellpadding="0" class="p-sitemapcol" width="100%">\n';
		p.output(html);
		doTable(p, i.toString(), 0);
		
		html='\
			</table>\n\
		</td>\n';
		if (Math.floor(i/3)==i/3) {
			html+='\
	</tr>\n\
	<tr>\n\
		<td colspan="5">'+p.img(1,30)+'</td>\n\
	</tr>\n\
	<tr>\n';
		} else {
			html+='\
		<td width="15">'+p.img(15,1)+'</td>\n';
		}
	}
	html+='\
	</tr>\n\
</table>\n';
	p.output(html);
}
_Page.prototype.endPage = function() {
	var p=this;
	
	//Begin: The page is not framed.
	if(p.framed!="1"){
	
		var ln = (p.showLeftNavigator()?"-ln":"");
		var j;
		function hasSubMenu(item,i,l,nw) {
			var html;
			var style;
	
			if (p.items[item+"_1"]!=null) {
				html='class="p-mism"';	
			} else {
				html='class="p-mi"';
			}
			style = (i==1?p.topItem:'');
			if (l=='' | l=='#' | l=='javascript:void(null)') {
				style += p.noLink;
			} else {
				style += p.nsHand;
			}
			html += (style==''?'':' style="'+style+'"');
			return html;
		}
		function subMenu(item) {
			var index;
			var i;
  			var s="";
  				
  			if (_page.ns!='1') {
				s+="<IFRAME frameborder=0 id=if-p-sm_"+item+" src=\""+_page.commonServer+"/img/t.gif\" scroll=none style=\"FILTER:progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0);visibility:hidden;height:0;position:absolute;width:0px;top:0px;z-index:10\"></iframe>"
			}
			s+='<div id="p-sm_'+item+'" class="p-sm">\n\
			<table border="0" cellspacing="0" cellpadding="0" width="100%" class="p-smt">\n';
  
			for (i=1;p.items[item+"_"+i.toString()]!=null;i++) {
				index=item+"_"+i.toString();
				s+='\
				<tr>\n\
				<td id="p-mi_'+index+'" '+hasSubMenu(index,i,p.items[index].link,p.items[index].nw)+' onmouseover="_MOver(this)" onmouseout="_MOut(this)" onclick="_MClick(this)">\n'
				s+= p.items[index].text;
				s+= '</td></tr>\n';
			}
			s+='\
			</table>\n\
			</div>\n';
			return s;
		}
		function createSubmenus(item) {
  	 		var i;
			if (p.items[item+"_1"]!=null) {
				p.output(subMenu(item));
				for (i=1;p.items[item+"_"+i.toString()]!=null;i++) {
					createSubmenus(item+"_"+i.toString());
				}
			}
		}
		var html='\n\n\
		<!-- end of content area -->\n\
      </td>\n\
		<td id="p-rightinside">&nbsp;</td>\n\
		<td id="p-right">&nbsp;</td>\n\
		</tr>\n\
		<tr id="p-row7" height="100%">\n\
		<td id="p-bottomleft">&nbsp;</td>\n\
		<td id="p-bottomleftcorner'+ln+'">&nbsp;</td>\n\
      <td class="p-bottom'+ln+'">&nbsp;</td>\n\
		<td class="p-bottom">&nbsp;</td>\n\
		<td class="p-bottom">&nbsp;</td>\n\
		<td class="p-bottom">&nbsp;</td>\n\
		<td class="p-bottom">&nbsp;</td>\n\
		</tr>\n\
		</table>\n';
		//End: The page is not framed.
	} else {
		//Begin: The page is not framed.
		var html='';
		//Do not hide the left & right columns for the D2Login Form.
		if(!document._fDIAMONDWebLogin){
			html+='</td></tr></table>';
			//Only display the main column.
			var cellArray1 = document.getElementById('p-carow1').cells;
			var cellArray2 = document.getElementById('p-carow2').cells;
			for(i=0;i<cellArray1.length;i++){
				if(cellArray1[i].width!=''){
					//Only the body does not have a width specified.
					cellArray1[i].style.display='none';
					cellArray2[i].style.display='none';
				} else {
					//document.body.style.padding='10px';
					cellArray2[i].style.width='100%';
				}
			}
		document.getElementById('p-carow1').style.display='none';
		//End: The page is framed.
		}
	}
	p.output(html)
	
	for (j=0;p.items[j.toString()]!=null;j++) {
       		createSubmenus(j.toString());
	}   
	if (p.trace) {
	   p.traceWindow.document.write('</pre><hr>'+p.traceVar+'<hr>')
	   p.traceWindow.document.write('</body></html>')
	   p.traceWindow.document.close()
	}
	
	p.pageready = true;
	p.pageScript();
	document.close();
}
_Page.prototype.printVersion = function(cellid){
	/* Temporary check to prevent non tool pages to receive errors in printversion */
	if(_page.callcount > 0){
		/* New Common Tool Printversion */
		_page.call('PrintVersion','cellid='+cellid);
	}else{
		/* Old Regular Printversion */
		var docTitle=window.document.title;
		var docHead=document.getElementsByTagName("head")[0].innerHTML;
        
		var docContent=document.getElementById(cellid).innerHTML;
		var prWindow;	
		prWindow = window.open('','thePrWindow','width=603,height=480,menubar,resizable,toolbar,scrollbars');
  		prWindow.document.open();
	  	var html=('\
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">\n\
		<html>\n\
		<head>'+docHead+'\n\
		</head>\n\
		<body class="p-printversion">');	
		if (_page.ns=='1') {	
			html+=('<table width="100%" border="0" cellpadding="0" cellspacing="0">\n\
				<tr id="p-row1" class="p-bg005aff">\n\
					<td width="20">'+this.img(20,1)+'</td>\n\
					<td></td>\n\
					<td width="20">'+this.img(20,1)+'</td>\n\
				</tr>\n\
				<tr id="p-row2" class="p-bg005aff">\n\
					<td>&nbsp;</td>\n\
					<td>\n\
						<table border="0" cellspacing="0" cellpadding="0" width="100%">\n\
							<tr>\n\
								<td style="text-align: right;">' + this.img(114, 21, 'KPMG Logo', null, 'KPMG-logo.png') + '</td>\n\
							</tr>\n\
						</table>\n\
					</td>\n\
					<td>&nbsp;</td>\n\
				</tr>\n\
				<tr id="p-row3" class="p-bg005aff">\n\
					<td colspan="3">&nbsp;</td>\n\
				</tr>\n\
			</table>');
		} else {	
			html+=('<table width="100%" cellpadding="0" cellspacing="0">\n\
				<tr id="p-row1" class="p-bg005aff">\n\
					<td width="20">'+this.img(20,1)+'</td>\n\
					<td></td>\n\
					<td width="20">'+this.img(20,1)+'</td>\n\
				</tr>\n\
				<tr id="p-row2" class="p-bg005aff">\n\
					<td>&nbsp;</td>\n\
					<td>\n\
						<table border="0" cellspacing="0" cellpadding="0" width="100%">\n\
							<tr>\n\
								<td style="text-align: right;">' + this.img(114, 21, 'KPMG Logo', null, 'KPMG-logo.png') + '</td>\n\
							</tr>\n\
						</table>\n\
					</td>\n\
					<td>&nbsp;</td>\n\
				</tr>\n\
				<tr id="p-row3" class="p-bg005aff">\n\
					<td colspan="3">&nbsp;</td>\n\
				</tr>\n\
			</table>');
		}	
		html+=('<table width="100%" cellpadding="0" cellspacing="0">\n\
		<tr>\n\
			<td width="20">'+this.img(20,1)+'</td>\n\
			<td>&nbsp;</td>\n\
			<td width="20">'+this.img(20,1)+'</td>\n\
		</tr>\n\
		<tr>\n\
			<td>&nbsp;</td>\n\
			<td>\n'+docContent+'</td>\n\
			<td>&nbsp;</td>\n\
		</tr>\n\
		<tr id="p-carow1">\n\
			<td colspan="3">&nbsp;</td>\n\
		</tr>\n\
		</table>\n\
		</body>\n\
		</html>');
		prWindow.document.write(html);
		prWindow.document.close();
		prWindow.focus();
	}
}
//*** Call Function ***
_Page.prototype.call = function(id,paramstring){
	if(!paramstring) var paramstring="";
		
	//This variable can be switched to false to prevent tools from loading.
	if(_page.loadTools==true){
	//Abstract parameters from the params string(s).
	var paramArray = paramstring.split(",");
	for(i=0;i<paramArray.length;i++){
  	   if(paramArray[i].substr(0,10)=="component="){
  	   	var component=paramArray[i].substr(10,paramArray[i].length-10);
  	   } 
	}
	
	//Check if this is an action or inline tool.
	if(_page.pageready==true) {
		//This is an action triggered by a link.
		var e = document.createElement("script");
      e.src = _page.commonTools+'/vwLkUpActions/'+id+'?open&type=action&params='+paramstring;
      e.type="text/javascript";
      document.getElementsByTagName("html")[0].appendChild(e);
	} else if(_page.pageready==false) {
		if(_page.callcount == 0){
			//This is the first tool that is rendered, we can now set the domain to kpmg.com
			try{window.document.domain="kpmg.com";}
			catch(e){}
		}
		//This is an inlined rendered tool.
		document.write("<div id='p-framediv'></div>")		
		if(!document.getElementById('p-framediv')){
			var e = document.createElement("div");
    	 	e.id = 'p-framediv';
     	 	e.type='text/html';
			e.style.visibility='hidden';
     	 	document.getElementsByTagName("head")[0].appendChild(e);
		}
				
		//This function handles the actual tool call.
		_page.callcount+=1;
		
		//Here we call the drawBox function.
		_page.drawBox(component,'',(id + _page.callcount),paramstring);
		
		//Here we create an instance of an HTTP Buffer (name: id1obj) and set the onLoad and onBeforeLoad handler.
		eval(id + _page.callcount + 'obj = new callBuffer( )');
		
		eval(id + _page.callcount + 'obj.onbeforeload = function( e ) {this.writeHTML("'+id + _page.callcount+'","<table width=\'100%\' height=\'80\' style=\'border:1px #d9d9d9 solid;margin-bottom:14px\'><tr><td><center>Loading content</center></td></tr></table>");}');
		eval(id + _page.callcount + 'obj.onerror = function( e ) {this.writeHTML("'+id + _page.callcount+'","<table width=\'100%\' height=\'80\' style=\'border:1px #d9d9d9 solid;margin-bottom:14px\'><tr><td><center>Error loading content</center></td></tr></table>");}');
		eval(id + _page.callcount + 'obj.onload = function( e ) {this.writeHTML("'+id + _page.callcount+'", this.getHTML());}');	
		//Here we set the the source URL and optionally the boxes it listens to.
		eval(id + _page.callcount + 'obj._source = "'+_page.commonTools+'/vwLkUpResults/'+id+'?open&params='+paramstring+'";');
		
		//Here we open the source URL.
		eval(id + _page.callcount + 'obj.getURL("'+_page.commonTools+'/vwLkUpResults/'+id+'?open&params='+paramstring+'");');
	}
	}
}
//*** CallBuffer Function ***
callBuffer.instances = [];
function callBuffer(){
	this.index = callBuffer.instances.length;
	callBuffer.instances[this.index] = this;
	this.ready			= false;
	this.loaded			= false;
	this.postload		= false;
	
	this.onload			= new Function();
	this.onbeforeload	= new Function();
	this._source		= new String();
	this._html			= new String();
	this._root			= null;
	this.createIFrame();
}
//*** CallBuffer/writeHTML Function ***
callBuffer.prototype.writeHTML = function(el,content){
	if(document.getElementById(el)){
		document.getElementById(el).innerHTML=content;
	} else {
		this.loaded=false;
		this.postload=true;
	}
}
//*** CallBuffer/createIFrame Function ***
callBuffer.prototype.createIFrame = function() {
	if(_page.ns!=1){
		this._iframe = document.createElement("<IFRAME style='position:relative;width:0px;height:0px' onload='callBuffer.instances[" + this.index + "].loadHandler()' name='RSFrame" + this.index+"'></IFRAME>");
		this._iframe = document.getElementById("p-framediv").appendChild( this._iframe );
	} else {
		this._iframe = document.createElement("IFRAME");
		this._iframe.name="RSFrame" + this.index;
		this._iframe.style.position = "relative";
		this._iframe.style.height = "0px";
		this._iframe.style.width = "0px";
		this._iframe.style.visibility = "hidden";
		document.body.appendChild( this._iframe );
	}
	this._iframe.onload = new Function("callBuffer.instances[" + this.index + "].loadHandler()");
	this.ready = true;
}
//*** CallBuffer/getURL Function ***
callBuffer.prototype.getURL = function ( url ) {
	this.loaded = false;
	this.onbeforeload();
	frames["RSFrame" + this.index].location.href = url;
}
//*** CallBuffer/clipHTML Function ***
callBuffer.prototype.clipHTML = function (htmlTemp) {
	preHTML="";
	//Check if the buffer contains a base href
	if(this._base) preHTML="<base href='"+this._base+"'/>";
	
	//Here we clip the content based on a specified tagname or id.
	if(this._clipid){
		return(preHTML + htmlTemp.getElementById(this._clipid).innerHTML);
	} else if(this._cliptag){
		return(preHTML + htmlTemp.getElementsByTagName(this._cliptag)[0].innerHTML);		
	} else {
		return(preHTML + htmlTemp.getElementsByTagName('form')[0].innerHTML);
	}
}
//*** CallBuffer/getHTML Function ***
callBuffer.prototype.getHTML = function () {
	var docNode = window.frames[this._iframe.name].document;		
	return (this.clipHTML(docNode));
}
//*** CallBuffer/redirectHandler Function ***
callBuffer.prototype.redirectHandler = function () {
		pNode = window.frames["RSFrame" + this.index].document.getElementById('RedirectSettings');
		cNode = pNode.firstChild;
		while(cNode){
			if(cNode.firstChild){
				eval ("this._" + cNode.id + " = \"" + cNode.firstChild.nodeValue + "\"");
			}
			cNode = cNode.nextSibling;
		}
		this.getURL(this._source);
}
//*** CallBuffer/loadHandler Function ***
callBuffer.prototype.loadHandler = function () {
	if (!this.loaded) {
		try{
			//Check if the first Node of the body is a redirect url.		
			if(window.frames["RSFrame" + this.index].document.getElementById('RedirectSettings')){
				this.loaded = false;
				this.redirectHandler();
			}	else {
				this.loaded = true;
				this.onload();
			}
		}
		
		catch(errObj){
			//If there is an error the frame is in accessible (wrong domain).
			//or the page resulted in an error.
			this.onerror()
		}	
	}
}
//*** drawBox Function ***
_Page.prototype.drawBox = function(component,content,id,params){
//Setting the standard variables
var textcolor='000000';
var bgcolor='d9e6ff';
var html='';
var title='';
//Setting up a parameter array.
paramArray = params.split(",");
//Abstracting the parameters from the params string(s).
for(i=0;i<paramArray.length;i++){
      if(paramArray[i].substr(0,6)=="title="){
            var title=paramArray[i].substr(6,paramArray[i].length-6);
      } 
      if(paramArray[i].substr(0,10)=="textcolor="){
            var textcolor=paramArray[i].substr(10,paramArray[i].length-10);
      } 
      if(paramArray[i].substr(0,8)=="bgcolor="){
            var bgcolor=paramArray[i].substr(8,paramArray[i].length-8);
      } 
}
if(component=='hl') {
   //Building the headline component
   html='<table border="0" cellpadding="0" cellspacing="0" class="p-hl">'+
   '<tr><td class="p-top" style="color:#'+textcolor+'">'+title+'</td></tr>'+
   '<tr><td class="p-content-000000" bgcolor="#'+bgcolor+'">'+content+'<div id ="'+id+'"></div>'+
   '</td></tr><tr><td class="p-bottom-'+bgcolor+'">&nbsp;</td></tr></table>'; 
} else if(component=='tool') {
   //Building the tool component
   html='<table border="0" cellpadding="0" cellspacing="0" class="p-tool">'+
   '<tr><td class="p-top-000000">'+title+'</td></tr><tr><td class="p-content">'+content+
	'<div id ="'+id+'"></div></td></tr><tr><td class="p-bottom">&nbsp;</td></tr>'+
   '</table>'; 
} else if(component=='rndportlet') {
   //Building the round portlet component
   html='<table border="0" cellpadding="0" cellspacing="0" class="p-rndportlet">'+content+
   '<tr><td class="p-top-99bdff">'+title+'</td></tr><tr><td class="p-content">'+
	'<div id ="'+id+'"></div></td></tr><tr><td class="p-bottom">&nbsp;</td></tr>'+
   '</table>'; 
} else if(component=='portlet') {
   //Building the portlet component
   html='<table border="0" cellpadding="0" cellspacing="0" class="p-portlet">'+
   '<tr><td class="p-top-99bdff">'+title+'</td></tr><tr><td class="p-content12">'+content+
   '<div id ="'+id+'"></div></td></tr><tr><td class="p-bottom">&nbsp;</td></tr>'+
   '</table>'; 
} else if(component=='mf') {
   //Building the main feature component
   html='<table border="0" cellpadding="0" cellspacing="0" class="p-mf"><tr>'+
   '<td class="p-content">'+content+'<div id ="'+id+'"></div></td></tr></table>'; 
} else if(component=='sf') {
   //Building the sub feature component
   html='<table border="0" cellpadding="0" cellspacing="0" class="p-sf"><tr>'+
   '<td class="p-top">'+title+'</td></tr><tr><td class="p-content">'+content+'<div id ="'+id+'">'+ 
   '</div></td></tr></table>'; 
} else {
   //Building only the div
   html+=content+'<div id ="'+id+'"></div>';
}
document.write(html);
}
function _MOverTrigger() {
	_page.menuactive = "yes"
	_MOver(_page.menuItemTemp)
}
function _MExpand(el) {
	if (_page.menuactive=="yes") {
		_page.menuItemTemp=el;
		_MOverTrigger();
	} else {
		_page.menuItemTemp=el;
		_page.timerOut=setTimeout('_MOverTrigger()',_page.delayExpand);
	}
}
function _MOverLH(el) {
	if (el.sn==null) {
		el.sn=document.getElementById('p-snleft');
	}
	if (el.lh==null) {
		el.lh=document.getElementById('p-mi_0');
	}
	if (el.sn!=null) {
		el.sn.style.backgroundColor='#F9F9FB';
		el.sn.style.backgroundImage='url('+_page.lhimgover.src+')';
		if(el.id="p-snleft"){
			document.getElementById('p-mi_0').style.backgroundImage='url('+_page.lhimgover.src+')';
		}
	}
	if (el.lh!=null) {
		id=el.lh.id.slice(el.lh.id.indexOf('_')+1);
		window.status = _page.items[id].link;
		el.lh.style.backgroundColor='#F9F9FB';
		el.lh.style.color="#000066";
	}
}
function _MOutLH(el) {
	window.status = '';
	if (el.sn==null) {
		el.sn=document.getElementById('p-snleft');
	}	
	if(el.lh==null) {
		el.lh=document.getElementById('p-mi_0');
	}
	if (el.sn!=null) {
		el.sn.style.backgroundColor='#E8E9F1';
		el.sn.style.backgroundImage='url('+_page.lhimgoff.src+')';
		if(el.id="p-snleft"){
			document.getElementById('p-mi_0').style.backgroundImage='url('+_page.lhimgoff.src+')';
		}
	}
	if (el.lh!=null) {
		el.lh.style.backgroundColor='#E8E9F1';
		el.lh.style.color="#3B4164";
	}
	el.className='p-snleft';
}
function _MClickLH(el) {
	if (el.lh==null) {
		el.lh=document.getElementById('p-mi_0');
	}
	_MClick(el.lh);
}
function _MOver(el) {	
	function getScrollX() {
		if (_page.ns=='1') {
			return window.pageXOffset;
		} else {
			if (document.compatMode && document.compatMode != 'BackCompat') {
					return document.documentElement.scrollLeft;
			} else {
					return document.body.scrollLeft;
			}
		}
	}
	function getScrollY() {
		if (_page.ns=='1') {
			return window.pageYOffset;
		} else {
			if (document.compatMode && document.compatMode != 'BackCompat') {
					return document.documentElement.scrollTop;
			} else {
					return document.body.scrollTop;
			}
		}
	}
	clearTimeout(_page.timerIn);
	var id = el.id.slice(el.id.indexOf('_')+1);
	var l = _page.items[id].link;
	if (l!='' & l!='#' & l!='javascript:void(null)') {
		window.status = l;
	}
	if (el.className!='p-tih') {
		//This is not the selected Item.
		el.style.backgroundColor="#F9F9FB";
		el.style.color="#000066";
		
		//Only perform this action for top menu items.
		if (el.id.length == 6) {
			el.style.backgroundImage='url('+_page.lhimgover.src+')';
		   if (el.id == "p-mi_0") {
			   lh=document.getElementById('p-snleft');
			   if (lh!=null) {
				   lh.style.backgroundImage='url('+_page.lhimgover.src+')';
			   	el.lh = lh;
			   }
		   }
		}
	}
	for (i=_page.listNr; i>0; i--) {
		l=_page.listSM[i-1].id.length<el.id.length?_page.listSM[i-1].id.length:el.id.length;
		if (_page.listSM[i-1].id.slice(4) != el.id.slice(4,l)) {
		
			_fixForm(true,_page.listSM[i-1]);
			_page.listSM[i-1].style.visibility='hidden';
			if(_page.listSM[i-1].pe.className!='p-tih') {
				_page.listSM[i-1].pe.style.backgroundColor="#E8E9F1";
				_page.listSM[i-1].pe.style.color="#3B4164";
				if (_page.listSM[i-1].pe.lh!=null) {
					_page.listSM[i-1].pe.lh.style.backgroundImage='url('+_page.lhimgoff.src+')';
				}
			}
			_page.listNr=i-1;
		}
	}
	if (_page.listNr>0) {
		if(_page.listSM[_page.listNr-1].pe.className!='p-tih') {
			_page.listSM[_page.listNr-1].pe.style.backgroundColor="#F9F9FB";
			_page.listSM[0].pe.style.color="#3B4164";
		}
	}
	if (el.sm==null) {
		el.sm = document.getElementById('p-sm_' + id);
		if (el.sm!=null) {
			if (_page.ns=='1') {
				var mi = document.getElementById('p-mi_'+id+'_1');
				el.sm.style.width=(mi.offsetWidth+2)+'px';
			}
			if (id.lastIndexOf('_') > -1) {
				el.pm = document.getElementById('p-sm_' + id.slice(0,id.lastIndexOf('_')));
			}
		}
	}
	if (el.sm!=null) {
		if (el.pm!=null) {
			var fmc=id.slice(id.length-2)=='_1'?1:0;
			el.sm.top = el.pm.top+el.offsetTop-fmc+3;
			if ((el.sm.top+el.sm.offsetHeight)>(_page.clientHeight+getScrollY())){
				el.sm.top=_page.clientHeight+getScrollY()-el.sm.offsetHeight;
			}
			if (el.pm.lft=='1') {
				el.sm.left = el.pm.left+el.offsetWidth-1;
				if ((el.sm.left+el.sm.offsetWidth)>_page.clientWidth-18+getScrollX()) {
					el.sm.left = el.pm.left-el.sm.offsetWidth+4;
					el.sm.lft = '0';
				} else {
					el.sm.lft = '1';
				}
			} else {
				el.sm.left = el.pm.left-el.sm.offsetWidth+4;
				if (el.sm.left<getScrollX()) {
					el.sm.left = el.pm.left+el.offsetWidth-1;
					el.sm.lft = '1';
				} else {
					el.sm.lft = '0';
				}
			}		
		} else {
			el.sm.top=131;
			el.sm.left=el.offsetLeft + (_page.ns=='1'&&_page.ns7!='1'?3:30)-4;
			el.sm.lft='1';
		}
		el.sm.style.top=el.sm.top+'px';
		el.sm.style.left=el.sm.left+'px';
	}
	if (el.sm!=null) {
		el.sm.style.visibility='visible';
		_page.listSM[_page.listNr] = el.sm;
		_page.listSM[_page.listNr].pe = el;
		_page.listNr++;
		
		_fixForm(false, el.sm);
	}
}
function _MOut(el) {
	window.status='';
	if (el.className!='p-tih') {
		el.style.backgroundColor="#E8E9F1";
		el.style.color="#3B4164";
		if (el.id.length == 6) {
			el.style.backgroundImage='url('+_page.lhimgoff.src+')';
		}
		
		if (el.lh != null) {
			el.lh.style.backgroundImage='url('+_page.lhimgoff.src+')';
		}
	}
	_page.timerIn=setTimeout(_clearMenu,_page.delayCollapse);
	clearTimeout(_page.timerOut);
}
function _MClick(el) {
	var id = el.id.slice(el.id.indexOf('_')+1);
	var l = _page.items[id].link;
	
	if (l!='' & l!='#' & l!='javascript:void(null)') {
		if (_page.ns=='1') {
			if (_page.items[id].nw==true) {
				window.open(l, '')
			} else if (_page.items[id].nw.replace(/ /g, "").indexOf('newwindow=yes')!=-1){
				window.open(l, '' , _page.items[id].nwProp)
			} else {	
				window.location=l;
			}
		} else {
			if (_page.items[id].nw==true || window.event.shiftKey || window.event.ctrlKey) {
				window.open(l, '')
			} else if (_page.items[id].nw.replace(/ /g, "").indexOf('newwindow=yes')!=-1){
				window.open(l, '' , _page.items[id].nwProp)
			} else {	
				window.location=l;
			}
		}
	}
}
function _clearMenu() {
	var lh;
	for (i=_page.listNr; i>0; i--) {
		_page.listSM[i-1].style.visibility='hidden';
		_fixForm(true,_page.listSM[i-1]);
		if(_page.listSM[i-1].pe.className!='p-tih') {
			_page.listSM[i-1].pe.style.backgroundColor='#E8E9F1';
			_page.listSM[i-1].pe.style.color='#3B4164';
			if(_page.listSM[i-1].pe.lh != null) {
				_page.listSM[i-1].pe.lh.className='p-snleft';
			}
		}
	}
	_page.listNr=0;
	_page.menuactive="no"
}
function _MOverRS(el) {
	clearTimeout(_page.rsTimer);
	el.style.color='#3B4164';
	el.style.background='#E4E6EF';
	window.status=_page.sites[parseInt(el.id.slice(el.id.indexOf('_')+1))].link;
}
function _MCloseRS(f) {
	window.status='';
	_page.rsTimer=setTimeout('_showHideRelatedSites('+f+')',_page.delayCollapse);
}
function _MOutRS(el) {
	el.style.color='#3B4164';
	el.style.background='#FFFFFF';
	window.status='';
	_page.rsTimer=setTimeout('_showHideRelatedSites()',_page.delayCollapse);
}
function _MClickRS(el) {
	var id = parseInt(el.id.slice(el.id.indexOf('_')+1));
	var l=_page.sites[id].link;
		if (_page.ns=='1') {
			if (_page.sites[id].nw==true) {
				window.open(l, '')
			} else {
				window.location=l;
			}
		} else {
			if (_page.sites[id].nw==true || window.event.shiftKey || window.event.ctrlKey) {
				window.open(l, '')
			} else {
				window.location=l;
			}
		}
}
function _showHideRelatedSites(f){
	function _findCoordinates(obj,axis)
	{
		var pos = 0;
		while (obj.offsetParent)
		{
			pos += (axis=='y')?obj.offsetTop:obj.offsetLeft;
			obj = obj.offsetParent;
		}
		return pos;
	}
	_page.rsLayer= document.getElementById('p-rsdiv');
	var imgObj=document.getElementById('p-relatedlayerplace');
	_page.rsLayer.style.left = (_findCoordinates(imgObj,'x'))+'px';
	_page.rsLayer.style.top = '90px';
	
	if (f==true){
		_page.rsLayer.style.visibility='hidden';
		if (_page.ns!='1') {
			_fixForm(true,document.getElementById('p-rsdiv'));
		}
	}else{
		if (_page.rsLayer.style.visibility == 'visible') {
			_page.rsLayer.style.visibility='hidden';
			if (_page.ns!='1') {
				_fixForm(true,document.getElementById('p-rsdiv'));
			}
		} else {
			_page.rsLayer.style.visibility='visible';
			if (_page.ns!='1') {
				_fixForm(false,document.getElementById('p-rsdiv'));
			}
		}
	}
}
/*
This protect menu's from sliding under select boxes.
 */
function _fixForm(show,menuObject){
	function getIEVersion() {
		var ua = navigator.userAgent;
		var IEOffset = ua.indexOf('MSIE ')
		return parseFloat(ua.substring(IEOffset + 5, ua.indexOf(';', IEOffset)))
	}
	if (_page.ns!='1') {
		if(getIEVersion() < 5.5) {
			//Do Nothing yet, we might hide all SELECT boxes.
		} else {
			if(show) {
				document.getElementById("if-"+menuObject.id).style.visibility='hidden';
			} else {
				document.getElementById("if-"+menuObject.id).style.visibility='visible';
				document.getElementById("if-"+menuObject.id).style.top=menuObject.offsetTop+"px";
				document.getElementById("if-"+menuObject.id).style.left=menuObject.offsetLeft+"px";
				document.getElementById("if-"+menuObject.id).style.height=menuObject.offsetHeight+"px";
				document.getElementById("if-"+menuObject.id).style.width=menuObject.offsetWidth+"px";
			}
		}
	}
}
/*
We need only a single instance of _Page class.
 */
var _page = new _Page();
/*
Needed for backwards compatibility 
*/
function _nop() {}
window.onload = _nop;
window.onresize = _nop;
