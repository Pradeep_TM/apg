﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KPMG.APGBuilder.Web.Models
{
    public class ActivityViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public ActivityViewModel() { }
        public ActivityViewModel(Activity activity)
        {
            if (activity != null)
            {
                this.Id = activity.Id;
                this.Name = activity.Name;
                this.Description = activity.Description;
                this.IsActive = activity.IsActive;
                if (activity.ActivityIndustryMappings != null && activity.ActivityIndustryMappings.Count > 0)
                {
                    SelectedIndustry = activity.ActivityIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    SubIndustry = string.Join(", ", activity.ActivityIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }
            this.Activity = activity;
        }


        public Activity Activity { get; set; }
        public long Id { get; set; }
        [Required(ErrorMessage = "Name is mandatory field")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Description is mandatory field")]
        public string Description { get; set; }
        [Required(ErrorMessage = "IsActive status is mandatory field")]
        public bool? IsActive { get; set; }


        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }


        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        //[Required(ErrorMessage = "Select one Industry")]
        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long BPId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.ActivityIndustryMappings
                              where industryMapping.SubIndustryId == industryId
                              && industryMapping.ActivityId == BPId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping

    }
}