﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
   public class ExecutionRiskIndustryMappingManager:EntityManager<Entity.ExecutionRiskIndustryMapping>
    {
       public ExecutionRiskIndustryMappingManager() : base() { }
       public ExecutionRiskIndustryMappingManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
       public ExecutionRiskIndustryMappingManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
