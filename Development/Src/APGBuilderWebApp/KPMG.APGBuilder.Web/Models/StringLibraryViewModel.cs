﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace KPMG.APGBuilder.Web.Models
{
    public class StringLibraryViewModel 
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public StringLibraryViewModel() { }
        public StringLibraryViewModel(StringLibrary stringLibrary)
        {
            if(stringLibrary!=null)
            {
                this.Id = stringLibrary.Id;
                this.BusinessProcessId = stringLibrary.BusinessProcessId;
                this.BusinessProcessName = stringLibrary.BusinessProcess.Name;
                this.ProcessId = stringLibrary.ProcessId;
                this.ProcessName = stringLibrary.Process.Name;
                this.SubProcessId = stringLibrary.SubProcessId;
                this.SubProcessName = stringLibrary.SubProcess.Name;
                this.ActivityId = stringLibrary.ActivityId;
                this.ActivityName = stringLibrary.Activity.Name;
                this.EnterpriseRiskId = stringLibrary.EnterpriseRiskId;
                this.EnterpriseRiskName = stringLibrary.EnterpriseRisk.Name;
                this.BusinessRiskId = stringLibrary.BusinessRiskId;
                this.BusinessRiskName = stringLibrary.BusinessRisk.Name;
                this.ExecutionRiskId = stringLibrary.ExecutionRiskId;
                this.EnterpriseRiskName = stringLibrary.ExecutionRisk.Name;
                this.ControlId = stringLibrary.ControlId;
                this.ControlName = stringLibrary.Control.Name;
                this.TestObjectiveId = stringLibrary.TestObjectiveId;
                this.TestObjectiveName = stringLibrary.TestObjective.Name;
                this.TestId = stringLibrary.TestId;
                this.TestName = stringLibrary.Test.Name;
                this.IsActive = stringLibrary.IsActive;
                this.ReviewStatus = stringLibrary.ReviewStatus;
                this.ApproveStatus = stringLibrary.ApproveStatus;
            }
            
            this.stringLibrary = stringLibrary;
        }


        public StringLibrary stringLibrary { get; set; }

        public long Id { get; set; }
       
        public long BusinessProcessId { get; set; }
        [DisplayName("Business Process")]
        public string BusinessProcessName { get; set; }
        public long ProcessId { get; set; }
         [DisplayName("Process")]
        public string ProcessName { get; set; }
        public long SubProcessId { get; set; }
         [DisplayName("Sub Process")]
        public string SubProcessName { get; set; }
        public long ActivityId { get; set; }
         [DisplayName("Activity")]
        public string ActivityName { get; set; }

        public long EnterpriseRiskId { get; set; }
         [DisplayName("Enterprise Risk")]
        public string EnterpriseRiskName { get; set; }
        public long BusinessRiskId { get; set; }
         [DisplayName("Business Risk")]
        public string BusinessRiskName { get; set; }
        public long ExecutionRiskId { get; set; }
         [DisplayName("Execution Risk")]
        public string ExecutionRiskName { get; set; }
        public long ControlId { get; set; }
         [DisplayName("Control")]
        public string ControlName { get; set; }
        public long TestObjectiveId { get; set; }
         [DisplayName("Test Objective")]
        public string TestObjectiveName { get; set; }
        public long TestId { get; set; }
         [DisplayName("Test")]
        public string TestName { get; set; }
        public bool? ReviewStatus { get; set; }
        public bool? ApproveStatus { get; set; }
        public bool? IsActive { get; set; }

        //private IList<BusinessProcess> _businessProcessList;
        //public IList<BusinessProcess> BusinessProcessList
        //{
        //    get
        //    {
        //        if(_businessProcessList==null)
        //        {
        //            _businessProcessList = new BusinessProcessManager().GetList();
        //        }

        //        return _businessProcessList;
        //    }
        //}

        //private IList<Process> _processList;
        //public IList<Process> ProcessList
        //{
        //    get
        //    {
        //        if(_processList==null)
        //        {
        //            _processList = new ProcessManager().GetList();
        //        }

        //        return _processList;
        //    }
        //}

        //private IList<SubProcess> _subProcessList;
        //public IList<SubProcess> SubProcessList
        //{
        //    get
        //    {
        //        if(_subProcessList==null)
        //        {
        //            _subProcessList = new SubProcessManager().GetList();
        //        }

        //        return _subProcessList;
        //    }
        //}

        //private IList<Activity> _activityList;
        //public IList<Activity> ActivityList
        //{
        //    get
        //    {
        //       if(_activityList==null)
        //       {
        //           _activityList = new ActivityManager().GetList();
        //       }
        //       return _activityList;
        //    }
        //}

        //private IList<EnterpriseRisk> _enterpriseRiskList;
        //public IList<EnterpriseRisk> EnterpriseRiskList
        //{
        //    get
        //    {
        //        if(_enterpriseRiskList==null)
        //        {
        //            _enterpriseRiskList = new EnterpriseRiskManager().GetList();
        //        }
        //        return _enterpriseRiskList;
        //    }
        //}

        //private IList<BusinessRisk> _businessRiskList;
        //public IList<BusinessRisk> BusinessRiskList
        //{
        //    get
        //    {
        //        if(_businessRiskList==null)
        //        {
        //            _businessRiskList = new BusinessRiskManager().GetList();
        //        }
        //        return _businessRiskList;
        //    }
        //}

        //private IList<ExecutionRisk> _executionRiskList;
        //public IList<ExecutionRisk> ExecutionRiskList
        //{
        //    get
        //    {
        //        if(_executionRiskList==null)
        //        {
        //            _executionRiskList = new ExecutionRiskManager().GetList();
        //        }
        //        return _executionRiskList;
        //    }
        //}

        //private IList<Control> _controlList;
        //public IList<Control> ControlList
        //{
        //    get
        //    {
        //        if(_controlList==null)
        //        {
        //            _controlList = new ControlManager().GetList();
        //        }
        //        return _controlList;
        //    }
        //}

        //private IList<TestObjective> _testObjectiveList;
        //public IList<TestObjective> TestObjectiveList
        //{
        //    get
        //    {
        //        if(_testObjectiveList==null)
        //        {
        //            _testObjectiveList = new TestObjectiveManager().GetList();
        //        }
        //        return _testObjectiveList;
        //    }
        //}

        //private IList<Test> _testList;
        //public IList<Test> TestList
        //{
        //    get
        //    {
        //        if(_testList==null)
        //        {
        //            _testList = new TestManager().GetList();
        //        }
        //        return _testList;
        //    }
        //}
    }
}