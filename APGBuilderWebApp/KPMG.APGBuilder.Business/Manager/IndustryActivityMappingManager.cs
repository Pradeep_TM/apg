﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class ActivityIndustryMappingManager : EntityManager<Entity.ActivityIndustryMapping>
    {
        public ActivityIndustryMappingManager() : base() { }
        public ActivityIndustryMappingManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public ActivityIndustryMappingManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
