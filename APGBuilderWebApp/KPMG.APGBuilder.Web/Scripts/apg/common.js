﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />

$(document).ready(function () {
    $('a', 'ul.secondaryNavigation').click(function (){
        var self = $(this);
        self.addClass('selected');
        $('a.selected', 'ul.secondaryNavigation').each(function ()
        {
            $(this).removeClass('selected');
        });
    });
});