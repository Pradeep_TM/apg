﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class BusinessProcessIndustryMappingManager : EntityManager<Entity.BusinessProcessIndustryMapping>
    {
        public BusinessProcessIndustryMappingManager() : base() { }
        public BusinessProcessIndustryMappingManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public BusinessProcessIndustryMappingManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
