﻿using System.Web;
using System.Web.Optimization;

namespace KPMG.APGBuilder.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Clear all items from the default ignore list to allow minified CSS and JavaScript files to be included in debug mode
            bundles.IgnoreList.Clear();
   
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/kendo/2015.1.408/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                        "~/Scripts/kendo/2015.1.408/kendo.all.min.js",
                        "~/Scripts/kendo/2015.1.408/kendo.core.min.js",
                        "~/Scripts/kendo/2015.1.408/kendo.aspnetmvc.min.js",
                        "~/Scripts/kendo/2015.1.408/kendo.timezones.min.js",
                        "~/Scripts/kendo/2015.1.408/kendo.web.min.js",
                        "~/Scripts/jquery.tokeninput.js"
                        ));


            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                        "~/Content/kendo/2015.1.408/kendo.common.min.css",
                        "~/Content/kendo/2015.1.408/kendo.rtl.min.css",
                        "~/Content/kendo/2015.1.408/kendo.default.min.css",
                        "~/css/KPMG_styles.css",
                        "~/css/ie.css", 
                        "~/css/token-input.css",
                        "~/css/developer.css"
                        ));


        
        }
    }
}
