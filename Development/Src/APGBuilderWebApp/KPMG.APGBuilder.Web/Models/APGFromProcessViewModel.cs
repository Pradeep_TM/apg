﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KPMG.APGBuilder.Web.Models
{
    public class APGFromProcessViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public APGFromProcessViewModel() { }


        #region Business Process Mapping

        private IList<BusinessProcess> _businessProcessList;

        public IList<BusinessProcess> BusinessProcessList
        {
            get
            {
                if (_businessProcessList == null)
                {
                    _businessProcessList = new BusinessProcessManager().GetList();
                }
                return _businessProcessList;
            }
        }

        #endregion

        #region Process Mapping

        private IList<Process> _processList;

        public IList<Process> ProcessList
        {
            get
            {
                if (_processList == null)
                {
                    _processList = new ProcessManager().GetList();
                }
                return _processList;
            }
        }

        #endregion

        #region SubProcess Mapping

        private IList<SubProcess> _subProcessList;

        public IList<SubProcess> SubProcessList
        {
            get
            {
                if (_subProcessList == null)
                {
                    _subProcessList = new SubProcessManager().GetList();
                }
                return _subProcessList;
            }
        }

        #endregion

        #region SubProcess Mapping

        private IList<Activity> _activityList;

        public IList<Activity> ActivityList
        {
            get
            {
                if (_activityList == null)
                {
                    _activityList = new ActivityManager().GetList();
                }
                return _activityList;
            }
        }

        #endregion

        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        [Required(ErrorMessage = "Select one Industry")]
        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long BPId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.ActivityIndustryMappings
                              where industryMapping.SubIndustryId == industryId
                              && industryMapping.ActivityId == BPId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping
    }
}