﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace KPMG.APGBuilder.Web.Models
{
    public class TestViewModel
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();

        public TestViewModel() { }

        public TestViewModel(Test test)
        {
            if(test != null)
            {
                this.Id = test.Id;
                this.Name = test.Name;
                this.Description = test.Description;
                this.Hypothesis = test.Hypothesis;
                this.Notes = test.Notes;
                this.Technique = test.Technique;
                this.System = test.System;
                this.SystemNotes = test.SystemNotes;
                this.EstimatedHours =  (decimal) test.EstimatedHours;
                this.Approved = test.Approved;
                this.IsActive = test.IsActive;

            }
            this.test = test;
        }

        public Test test { get; set; }

        public long Id { get; set; }

        [Required(ErrorMessage = "Test Objective Name is Required...")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Hypothesis { get; set; }

        public string Notes { get; set; }

        public string Technique { get; set; }

        public string System { get; set; }

        public string SystemNotes { get; set; }

        public decimal EstimatedHours { get; set; }

        public bool? Approved { get; set; }

        public bool? IsActive { get; set; }

        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        private List<string> _systemList;
        public List<string> SystemList
        {
            get
            {
                if (_systemList == null)
                {
                    _systemList = new TestManager().GetList().Select(test=> test.System).Distinct().ToList();
                }
                return _systemList;
            }
        }

        private List<string> _techniqueList;
        public List<string> TechniqueList
        {
            get
            {
                if (_techniqueList == null)
                {
                    _techniqueList = new TestManager().GetList().Select(test => test.Technique).Distinct().ToList();
                }
                return _techniqueList;
            }
        }   
    }
}