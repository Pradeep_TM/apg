﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace KPMG.APGBuilder.Web.Controllers
{
    public class UserController : GridEntityController<User>
    {
        public UserController() : base(new UserManager()) { }

        public override ActionResult Index()
        {
            IList<User> userlist = Manager.GetList();
            return View(GetViewModel(userlist));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }


        public override ActionResult Create()
        {
            return base.Create();
        }

       

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            User userObject = Manager.New();
            UserViewModel viewModel = new UserViewModel();
            TryUpdateModel(viewModel);
            userObject.Name = viewModel.Name;
            userObject.CorpId = viewModel.CorpId;
            userObject.IsActive = viewModel.IActive;
            userObject.CreatedBy = Session["UserName"].ToString();
            userObject.CreatedOn = DateTime.Now;
            Manager.Create(userObject);

            return View();
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            User userObject = Manager.Read(id);
            UserViewModel viewModel = new UserViewModel();
            TryUpdateModel(viewModel);
            userObject.Name = viewModel.Name;
            userObject.CorpId = viewModel.CorpId;
            userObject.IsActive = viewModel.IActive;
            userObject.ModifiedBy = Session["UserName"].ToString();
            userObject.ModifiedOn = DateTime.Now;
            Manager.Update(userObject);

            return View();
        }


        public override ActionResult Delete(long id)
        {
            return View();
        }


        protected override object GetViewModel(User result)
        {
            return new UserViewModel(result);
        }

        protected override bool TryUpdateViewModel(User result)
        {
            return TryUpdateModel(new UserViewModel(result));
        }

        protected override System.Collections.IEnumerable GetViewModel(IEnumerable<User> results)
        {
            List<UserViewModel> viewModel = new List<UserViewModel>();
            foreach (var user in results)
            {
                viewModel.Add(new UserViewModel {Id=user.Id, Name=user.Name, IActive=user.IsActive });
            }
            return viewModel.ToArray();
        }
       
    }
}
