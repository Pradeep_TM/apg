﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace KPMG.APGBuilder.Web.Models
{
    public class StringLibrarySingleItemBuilderViewModel
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public StringLibrarySingleItemBuilderViewModel() { }
        public StringLibrarySingleItemBuilderViewModel(StringLibrary stringLibrary)
        {
            if (stringLibrary != null)
            {
                this.StringLibrarySingleItem.BusinessProcessId = stringLibrary.BusinessProcessId;
                this.StringLibrarySingleItem.BusinessProcessName = stringLibrary.BusinessProcess.Name;
                this.StringLibrarySingleItem.ProcessId = stringLibrary.ProcessId;
                this.StringLibrarySingleItem.ProcessName = stringLibrary.Process.Name;
                this.StringLibrarySingleItem.SubProcessId = stringLibrary.SubProcessId;
                this.StringLibrarySingleItem.SubProcessName = stringLibrary.SubProcess.Name;
                this.StringLibrarySingleItem.ActivityId = stringLibrary.ActivityId;
                this.StringLibrarySingleItem.ActivityName = stringLibrary.Activity.Name;
                this.StringLibrarySingleItem.EnterpriseRiskId = stringLibrary.EnterpriseRiskId;
                this.StringLibrarySingleItem.EnterpriseRiskName = stringLibrary.EnterpriseRisk.Name;
                this.StringLibrarySingleItem.BusinessRiskId = stringLibrary.BusinessRiskId;
                this.StringLibrarySingleItem.BusinessRiskName = stringLibrary.BusinessRisk.Name;
                this.StringLibrarySingleItem.ExecutionRiskId = stringLibrary.ExecutionRiskId;
                this.StringLibrarySingleItem.EnterpriseRiskName = stringLibrary.ExecutionRisk.Name;
                this.StringLibrarySingleItem.ControlId = stringLibrary.ControlId;
                this.StringLibrarySingleItem.ControlName = stringLibrary.Control.Name;
                this.StringLibrarySingleItem.TestObjectiveId = stringLibrary.TestObjectiveId;
                this.StringLibrarySingleItem.TestObjectiveName = stringLibrary.TestObjective.Name;
                this.StringLibrarySingleItem.TestId = stringLibrary.TestId;
                this.StringLibrarySingleItem.TestName = stringLibrary.Test.Name;
            }
        }


        private IList<StringLibraryViewModel> _stringLibraryList;
        public IList<StringLibraryViewModel> StringLibraryList
        {
            get
            {
                if (_stringLibraryList == null)
                {
                    _stringLibraryList = new StringLibraryManager().GetList().Select(library=> new StringLibraryViewModel(library)).ToList();
                }
                return _stringLibraryList;
            }
        }

        private StringLibraryViewModel _stringLibrarySingleItem;
        public StringLibraryViewModel StringLibrarySingleItem
        {
            get
            {
                return _stringLibrarySingleItem;
            }
            set { _stringLibrarySingleItem = value; }
        }

        //public StringLibraryViewModel StringLibrarySingleItem = new StringLibraryViewModel();

        //private StringLibraryViewModel _stringLibrarySingleItem;
        //public StringLibraryViewModel StringLibrarySingleItem
        //{
        //    get
        //    {
        //        if (_stringLibrarySingleItem == null)
        //        {
        //            _stringLibrarySingleItem = new StringLibraryManager().New();
        //        }
        //        return _stringLibrarySingleItem;
        //    }
        //}

       
    }
}