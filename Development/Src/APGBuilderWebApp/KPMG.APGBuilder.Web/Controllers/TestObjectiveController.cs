﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using KPMG.APGBuilder.Web.AppCode;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class TestObjectiveController : GridEntityController<TestObjective>
    {
        //
        // GET: /TestObjective/

        public TestObjectiveController() : base(new TestObjectiveManager()) { }

        public override ActionResult Index()
        {
            IList<TestObjective> testObjectiveList = Manager.GetList();
            ViewBag.Controller = APGConstants.TEST_OBJECTIVE;
            return View(GetViewModel(testObjectiveList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }


        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            TestObjective bpObject = Manager.New();
            TestObjectiveViewModel viewModel = new TestObjectiveViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.CreatedBy = Session["UserName"].ToString();
                bpObject.CreatedOn = DateTime.Now;
                Manager.Create(bpObject);
                viewModel.Id = bpObject.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            TestObjective bpObject = Manager.Read(id);
            TestObjectiveViewModel viewModel = new TestObjectiveViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.ModifiedBy = Session["UserName"].ToString();
                bpObject.ModifiedOn = DateTime.Now;
                Manager.Update(bpObject);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            TestObjective bpObject = Manager.Read(id);
            bpObject.IsActive = false;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            TestObjective testObjectivey = Manager.Read(id);
            testObjectivey.IsActive = true;
            Manager.Update(testObjectivey);
            return RedirectToAction(APGConstants.INDEX);
        }

        #region TestObjective Industry Mapping
        private void CreateProjectForStandard(TestObjectiveViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {

                        dataContext.TestObjectiveIndustryMappings.InsertOnSubmit(new TestObjectiveIndustryMapping
                        {
                            TestObjectiveId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Session["UserName"].ToString(),
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        private void UpdateProjectForStandard(TestObjectiveViewModel model)
        {
            var businessRisk = Manager.Read(model.Id);
            var existingIds = businessRisk.TestObjectiveIndustryMappings.Select(x => x.SubIndustryId).Distinct().ToList();

            if (model.SelectedIndustry == null)
            {
                // if the industry mapping is not available
                model.SelectedIndustry = new List<long>().ToArray();
            }

            var deletedSubIndustryIds = existingIds.Except(model.SelectedIndustry).ToList();
            var addedSubIndustryIds = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                //Delete the subindustry mappings which were unselected
                if (deletedSubIndustryIds.Count > 0)
                {
                    var subIndustryMappings = dataContext.TestObjectiveIndustryMappings.Where(x => x.TestObjectiveId == model.Id && deletedSubIndustryIds.Contains(x.SubIndustryId))
                                                                                    .ToList();
                    dataContext.TestObjectiveIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                //Add new sub industries
                if (addedSubIndustryIds.Count > 0)
                {
                    addedSubIndustryIds.ForEach(selIndustry =>
                    dataContext.TestObjectiveIndustryMappings.InsertOnSubmit(new TestObjectiveIndustryMapping
                    {
                        TestObjectiveId = model.Id,
                        SubIndustryId = selIndustry,
                        CreatedBy = Convert.ToString(Session["User"]),
                        CreatedOn = DateTime.Now
                    })
                    );
                }

                dataContext.SubmitChanges();
            }
        }

        #endregion BusinessProcess Industry Mapping

        protected override object GetViewModel(TestObjective result)
        {
            return new TestObjectiveViewModel(result);
        }

        protected override bool TryUpdateViewModel(TestObjective result)
        {
            return TryUpdateModel(new TestObjectiveViewModel(result));
        }


        protected override System.Collections.IEnumerable GetViewModel(IEnumerable<TestObjective> results)
        {
            var testObjectives = results.Select(testObjective => new TestObjectiveViewModel(testObjective)).ToArray();
            return testObjectives;
        }
    }
}
