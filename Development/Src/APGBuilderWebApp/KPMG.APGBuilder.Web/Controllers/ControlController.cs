﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using KPMG.APGBuilder.Web.AppCode;


namespace KPMG.APGBuilder.Web.Controllers
{
    public class ControlController : GridEntityController<Control>
    {
        public ControlController() : base(new ControlManager()) { }

        public override ActionResult Index()
        {
            IList<Control> ControlList = Manager.GetList();
            ViewBag.Controller = APGConstants.CONTROL;
            return View(GetViewModel(ControlList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }

        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            Control control = Manager.New();
            ControlViewModel viewModel = new ControlViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                control.Name = viewModel.Name;
                control.Description = viewModel.Description;
                control.IsActive = viewModel.IsActive;
                control.CreatedBy = Session["UserName"].ToString();
                control.CreatedOn = DateTime.Now;
                Manager.Create(control);
                viewModel.Id = control.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection form)
        {
            Control control = Manager.Read(id);
            ControlViewModel viewModel = new ControlViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                control.Name = viewModel.Name;
                control.Description = viewModel.Description;
                control.IsActive = viewModel.IsActive;
                control.ModifiedBy = Session["UserName"].ToString();
                control.ModifiedOn = DateTime.Now;
                Manager.Update(control);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }

            return View(viewModel);
        }

        public override ActionResult Delete(long id)
        {
            Control control = Manager.Read(id);
            control.IsActive = false;
            Manager.Update(control);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            Control control = Manager.Read(id);
            control.IsActive = true;
            Manager.Update(control);
            return RedirectToAction(APGConstants.INDEX);
        }

        private void CreateProjectForStandard(ControlViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {
                        dataContext.ControlIndustryMappings.InsertOnSubmit(new ControlIndustryMapping
                        {
                            ControlId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Session["UserName"].ToString(),
                            CreatedOn = DateTime.Now
                        });

                        dataContext.SubmitChanges();
                    }
                }
            }
        }

        private void UpdateProjectForStandard(ControlViewModel model)
        {
            var control = Manager.Read(model.Id);
            var existingIds = control.ControlIndustryMappings.Select(cntrl => cntrl.SubIndustryId).Distinct().ToList();

            if (model.SelectedIndustry == null)
            {
                // if the industry mapping is not available
                model.SelectedIndustry = new List<long>().ToArray();
            }

            var subIndustryToBeDeleted = existingIds.Except(model.SelectedIndustry).ToList();
            var subIndustryToBeAdded = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (subIndustryToBeDeleted.Count > 0)
                {
                    var subIndustryMappings = dataContext.ControlIndustryMappings.Where(cntrl => cntrl.ControlId.Equals(model.Id) && subIndustryToBeDeleted.Contains(cntrl.SubIndustryId)).ToList();
                    dataContext.ControlIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                if (subIndustryToBeAdded.Count > 0)
                {
                    subIndustryToBeAdded.ForEach(selIndustry =>
                    dataContext.ControlIndustryMappings.InsertOnSubmit(new ControlIndustryMapping
                    {
                        ControlId = model.Id,
                        SubIndustryId = selIndustry,
                        CreatedBy = Convert.ToString(Session["UserName"]),
                        CreatedOn = DateTime.Now
                    })
                    );
                }

                dataContext.SubmitChanges();
            }
        }

        protected override object GetViewModel(Control result)
        {
            return new ControlViewModel(result);
        }

        protected override bool TryUpdateViewModel(Control entity)
        {
            return TryUpdateModel(new ControlViewModel(entity));
        }

        protected override IEnumerable GetViewModel(IEnumerable<Control> results)
        {
            var controls = results.Select(control => new ControlViewModel(control)).ToArray();
            return controls;
        }


    }
}
