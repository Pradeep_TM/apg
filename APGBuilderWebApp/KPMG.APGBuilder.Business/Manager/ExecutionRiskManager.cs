﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class ExecutionRiskManager : EntityManager<Entity.ExecutionRisk>
    {
        public ExecutionRiskManager() : base() { }
        public ExecutionRiskManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public ExecutionRiskManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
