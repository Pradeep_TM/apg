﻿function Details_Drildown(DivName, Sector, Process, Period) {
    var oTDL = '#td' + DivName + "_L";
    var oTDR = '#td' + DivName + "_R";
    var oImg = '#img' + DivName;
    var oImgSrc = document.getElementById('img' + DivName).src
    if ($(oTDL).is(":hidden") && $(oTDR).is(":hidden")) {
        $("#" + DivName + "_L").empty().html('<img src="/Content/ajax-loading-1.gif" />');
        $(oImg).attr("src", "/Content/details_close.png");
        $(oTDL).show("slow");
        $(oTDR).show("slow");
        $.ajax(
                {
                    type: "GET",
                    cache: false,
                    url: "/SubProcessComplianceSummarySectorwise/Index",
                    data: "process=" + Process + "&period=" + Period + "&sector=" + Sector,
                    dataType: "html",
                    success: function (result) {
                        $("#" + DivName + "_L").empty().html(result.split("«brak»")[0]);
                        $("#" + DivName + "_R").empty().html(result.split("«brak»")[1]);
                        SetColumnWidth("SubProcessCompliance_R_" + Process);
                    }
                }
            );
    }
    else {
        $(oImg).attr("src", "/Content/details_open.png");
        $(oTDL).hide("slow");
        $(oTDR).hide("slow");
    }
}


function Details_DrildownSubProcess(DivName, Sector, SubProcessId, Period) {
    var oTDL = '#td' + DivName + "_L";
    var oTDR = '#td' + DivName + "_R";
    var oImg = '#img' + DivName;
    var oImgSrc = document.getElementById('img' + DivName).src
    if ($(oTDL).is(":hidden")) {
        $("#" + DivName + "_L").empty().html('<img src="/Content/ajax-loading-1.gif" />');
        $(oImg).attr("src", "/Content/details_close.png");
        $(oTDL).show("slow");
        $(oTDR).show("slow");
        $.ajax(
                {
                    type: "GET",
                    cache: false,
                    url: "/ScenarioComplianceSummarySectorwise/Index",
                    data: "SubProcessId=" + SubProcessId + "&period=" + Period + "&sector=" + Sector,
                    dataType: "html",
                    success: function (result) {
                        $("#" + DivName + "_L").empty().html(result.split("«brak»")[0]);
                        $("#" + DivName + "_R").empty().html(result.split("«brak»")[1]);
                        SetColumnWidth("ScenarioProcessCompliance_R_" + SubProcessId);
                    }
                }
            );
    }
    else {
        $(oImg).attr("src", "/Content/details_open.png");
        $(oTDL).hide("slow");
        $(oTDR).hide("slow");
    }
}

function SetColumnWidth(ObjTbl) {
    try {
        var firstHeaderRow = document.getElementById('ProcessCompliance').rows[0];
        var ObjTblRight = document.getElementById(ObjTbl);

        var colsNum = firstHeaderRow.cells.length;
        for (var i = 0; i < colsNum; i++) {
            var cellWidth = firstHeaderRow.cells[i].offsetWidth-2;
            ObjTblRight.rows[0].cells[i].style.width = cellWidth + "px";
        }
        var ObjTblLeft = document.getElementById(ObjTbl.replace("_R_", "_L_"));
        var rowsNum = ObjTblLeft.rows.length;
        for (var i = 0; i < rowsNum; i++) {
            var cellHeight = ObjTblLeft.rows[i].cells[0].offsetHeight-2;
            ObjTblRight.rows[i].cells[0].style.height = cellHeight + "px";
        }
    } catch (ex) {
        //alert(ex)
    }
}
