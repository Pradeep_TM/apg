﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class ControlIndustryMappingManager : EntityManager<Entity.Control>
    {
        public ControlIndustryMappingManager() : base() { }
        public ControlIndustryMappingManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public ControlIndustryMappingManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
