﻿using KPMG.APGBuilder.Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPMG.APGBuilder.Web.Models
{
    public class UserViewModel
    {
        public UserViewModel() { }
        public UserViewModel(User user)
        {
            if (user != null)
            {
                this.Id = user.Id;
                this.Name = user.Name;
                this.CorpId = user.CorpId;
                //this.EmailAddress = user.EmailAddress;
                //this.IActive = user.IsActive;
            }
            this.User = user;
        }


        public User User { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string CorpId { get; set; }
        public string EmailAddress { get; set; }
        public bool? IActive { get; set; }

    }
}