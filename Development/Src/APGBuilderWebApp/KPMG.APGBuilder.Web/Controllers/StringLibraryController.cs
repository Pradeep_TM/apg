﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.APGBuilder.Web.AppCode;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace KPMG.APGBuilder.Web.Controllers
{
    public class StringLibraryController : GridEntityController<StringLibrary>
    {
        public StringLibraryController() : base(new StringLibraryManager()) { }


        public override ActionResult Index()
        {
            // IList<StringLibrary> stringLibraryList = Manager.GetList();
            StringLibrarySingleItemBuilderViewModel strSingleItem = new StringLibrarySingleItemBuilderViewModel();
            ViewBag.Controller = APGConstants.STRING_LIBRARY;
            //strSingleItem.StringLibraryList = strSingleItem.StringLibraryList.Select(x => new StringLibraryViewModel(x)).ToList();
            // return View(stringLibraryList);
            //return View(GetViewModel(stringLibraryList));
            return View(strSingleItem);
        }


        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            StringLibrary stringLibrary = Manager.New();
            StringLibrarySingleItemBuilderViewModel viewModel = new StringLibrarySingleItemBuilderViewModel();
            //StringLibraryViewModel viewModel = new StringLibraryViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                ////stringLibrary.BusinessProcessId = viewModel.BusinessProcessId;
                ////stringLibrary.ProcessId = viewModel.ProcessId;
                ////stringLibrary.SubProcessId = viewModel.SubProcessId;
                ////stringLibrary.ActivityId = viewModel.ActivityId;
                ////stringLibrary.EnterpriseRiskId = viewModel.EnterpriseRiskId;
                ////stringLibrary.BusinessProcessId = viewModel.BusinessProcessId;
                ////stringLibrary.ExecutionRiskId = viewModel.ExecutionRiskId;
                ////stringLibrary.ControlId = viewModel.ControlId;
                ////stringLibrary.TestObjectiveId = viewModel.TestObjectiveId;
                ////stringLibrary.TestId = viewModel.TestId;
                ////Manager.Create(stringLibrary);
                ////viewModel.Id = stringLibrary.Id;

                stringLibrary.BusinessProcessId = viewModel.StringLibrarySingleItem.BusinessProcessId;
                stringLibrary.ProcessId = viewModel.StringLibrarySingleItem.ProcessId;
                stringLibrary.SubProcessId = viewModel.StringLibrarySingleItem.SubProcessId;
                stringLibrary.ActivityId = viewModel.StringLibrarySingleItem.ActivityId;
                stringLibrary.EnterpriseRiskId = viewModel.StringLibrarySingleItem.EnterpriseRiskId;
                stringLibrary.BusinessProcessId = viewModel.StringLibrarySingleItem.BusinessProcessId;
                stringLibrary.ExecutionRiskId = viewModel.StringLibrarySingleItem.ExecutionRiskId;
                stringLibrary.ControlId = viewModel.StringLibrarySingleItem.ControlId;
                stringLibrary.TestObjectiveId = viewModel.StringLibrarySingleItem.TestObjectiveId;
                stringLibrary.TestId = viewModel.StringLibrarySingleItem.TestId;
                Manager.Create(stringLibrary);
                viewModel.StringLibrarySingleItem.Id = stringLibrary.Id;

            }
            return View(viewModel);
        }

        private long GetBusinessProcessId(string businessProcessName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.BusinessProcess.Name.Equals(businessProcessName, StringComparison.InvariantCultureIgnoreCase)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.BusinessProcess.Name.IsCaseInsensitiveEqual(businessProcessName)).BusinessProcessId;
                }
                else
                {
                    BusinessProcessManager manager = new BusinessProcessManager();
                    BusinessProcess businessProcess = manager.New();
                    businessProcess.Name = businessProcess.Description = businessProcessName;
                    businessProcess.CreatedBy = Convert.ToString(Session["UserName"]);
                    businessProcess.CreatedOn = DateTime.Now;
                    manager.Create(businessProcess);
                    return businessProcess.Id;
                }
            }
        }

        private long GetProcessId(string processName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.Process.Name.IsCaseInsensitiveEqual(processName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.Process.Name.IsCaseInsensitiveEqual(processName)).ProcessId;
                }
                else
                {
                    ProcessManager manager = new ProcessManager();
                    Process process = manager.New();
                    process.Name = process.Description = processName;
                    process.CreatedBy = Convert.ToString(Session["UserName"]);
                    process.CreatedOn = DateTime.Now;
                    manager.Create(process);
                    return process.Id;
                }
            }
        }

        private long GetSubProcessId(string subProcessName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.SubProcess.Name.IsCaseInsensitiveEqual(subProcessName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.SubProcess.Name.IsCaseInsensitiveEqual(subProcessName)).SubProcessId;
                }
                else
                {
                    SubProcessManager manager = new SubProcessManager();
                    SubProcess subProcess = manager.New();
                    subProcess.Name = subProcess.Description = subProcessName;
                    subProcess.CreatedBy = Convert.ToString(Session["UserName"]);
                    subProcess.CreatedOn = DateTime.Now;
                    manager.Create(subProcess);
                    return subProcess.Id;
                }
            }
        }

        private long GetActivityId(string activityName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.Activity.Name.IsCaseInsensitiveEqual(activityName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.Activity.Name.IsCaseInsensitiveEqual(activityName)).ActivityId;
                }
                else
                {
                    ActivityManager manager = new ActivityManager();
                    Activity activity = manager.New();
                    activity.Name = activity.Description = activityName;
                    activity.CreatedBy = Convert.ToString(Session["UserName"]);
                    activity.CreatedOn = DateTime.Now;
                    manager.Create(activity);
                    return activity.Id;
                }
            }
        }

        private long GetEnterpriseRiskId(string enterpriseRiskName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.EnterpriseRisk.Name.IsCaseInsensitiveEqual(enterpriseRiskName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.EnterpriseRisk.Name.IsCaseInsensitiveEqual(enterpriseRiskName)).EnterpriseRiskId;
                }
                else
                {
                    EnterpriseRiskManager manager = new EnterpriseRiskManager();
                    EnterpriseRisk enterpriseRisk = manager.New();
                    enterpriseRisk.Name = enterpriseRisk.Description = enterpriseRiskName;
                    enterpriseRisk.CreatedBy = Convert.ToString(Session["UserName"]);
                    enterpriseRisk.CreatedOn = DateTime.Now;
                    manager.Create(enterpriseRisk);
                    return enterpriseRisk.Id;
                }
            }
        }

        private long GetBusinessRiskId(string businessRiskName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.BusinessRisk.Name.IsCaseInsensitiveEqual(businessRiskName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.BusinessRisk.Name.IsCaseInsensitiveEqual(businessRiskName)).BusinessRiskId;
                }
                else
                {
                    BusinessRiskManager manager = new BusinessRiskManager();
                    BusinessRisk businessRisk = manager.New();
                    businessRisk.Name = businessRisk.Description = businessRiskName;
                    businessRisk.CreatedBy = Convert.ToString(Session["UserName"]);
                    businessRisk.CreatedOn = DateTime.Now;
                    manager.Create(businessRisk);
                    return businessRisk.Id;
                }
            }
        }

        private long GetExecutionRiskId(string executionRiskName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.ExecutionRisk.Name.IsCaseInsensitiveEqual(executionRiskName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.BusinessRisk.Name.IsCaseInsensitiveEqual(executionRiskName)).ExecutionRiskId;
                }
                else
                {
                    ExecutionRiskManager manager = new ExecutionRiskManager();
                    ExecutionRisk executionRisk = manager.New();
                    executionRisk.Name = executionRisk.Description = executionRiskName;
                    executionRisk.CreatedBy = Convert.ToString(Session["UserName"]);
                    executionRisk.CreatedOn = DateTime.Now;
                    manager.Create(executionRisk);
                    return executionRisk.Id;
                }
            }
        }

        private long GetControlId(string controlName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.Control.Name.IsCaseInsensitiveEqual(controlName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.Control.Name.IsCaseInsensitiveEqual(controlName)).ControlId;
                }
                else
                {
                    ControlManager manager = new ControlManager();
                    Control control = manager.New();
                    control.Name = control.Description = controlName;
                    control.CreatedBy = Convert.ToString(Session["UserName"]);
                    control.CreatedOn = DateTime.Now;
                    manager.Create(control);
                    return control.Id;
                }
            }
        }

        private long GetTestObjectiveId(string testObjectiveName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.TestObjective.Name.IsCaseInsensitiveEqual(testObjectiveName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.TestObjective.Name.IsCaseInsensitiveEqual(testObjectiveName)).TestObjectiveId;
                }
                else
                {
                    TestObjectiveManager manager = new TestObjectiveManager();
                    TestObjective testObjective = manager.New();
                    testObjective.Name = testObjective.Description = testObjectiveName;
                    testObjective.CreatedBy = Convert.ToString(Session["UserName"]);
                    testObjective.CreatedOn = DateTime.Now;
                    manager.Create(testObjective);
                    return testObjective.Id;
                }
            }
        }

        private long GetTestId(string testName)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (dataContext.StringLibraries.Where(library => library.Test.Name.IsCaseInsensitiveEqual(testName)).Any())
                {
                    return dataContext.StringLibraries.Single(library => library.Test.Name.IsCaseInsensitiveEqual(testName)).TestId;
                }
                else
                {
                    TestManager manager = new TestManager();
                    Test test = manager.New();
                    test.Name = test.Description = testName;
                    test.CreatedBy = Convert.ToString(Session["UserName"]);
                    test.CreatedOn = DateTime.Now;
                    manager.Create(test);
                    return test.Id;
                }
            }
        }

        #region JsonMethods_Token input
        [HttpGet]
        public JsonResult SearchBusinessProcess(string searchBP)
        {
            StringLibraryViewModel stringLibraryViewModel = new StringLibraryViewModel();
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.BusinessProcesses.ToList().FindAll(bp => bp.Name.ToLower().Contains(searchBP.ToLower()));
            // var searchResults = stringLibraryViewModel.BusinessProcessList.ToList().FindAll(bp => bp.Name.ToLower().Contains(searchBP.ToLower()));

            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchProcess(string searchProcess)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.Processes.ToList().FindAll(proc => proc.Name.ToLower().Contains(searchProcess.ToLower()));
            var jsonResult = searchResults.Select(results => new { ProcessId = results.Id, ProcessName = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchActivity(string searchActivity)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.Activities.ToList().FindAll(proc => proc.Name.ToLower().Contains(searchActivity.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchEnterpriseRisk(string searchER)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.EnterpriseRisks.ToList().FindAll(risk => risk.Name.ToLower().Contains(searchER.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchBusinessRisk(string searchBR)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.BusinessRisks.ToList().FindAll(risk => risk.Name.ToLower().Contains(searchBR.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchExecutionRisk(string searchER)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.ExecutionRisks.ToList().FindAll(risk => risk.Name.ToLower().Contains(searchER.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchControl(string searchControl)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.Controls.ToList().FindAll(risk => risk.Name.ToLower().Contains(searchControl.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchTestObjective(string searchTO)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.TestObjectives.ToList().FindAll(risk => risk.Name.ToLower().Contains(searchTO.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchTests(string searchTest)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.Tests.ToList().FindAll(risk => risk.Name.ToLower().Contains(searchTest.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchTestSteps(string searchTS)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.TestSteps.ToList().FindAll(risk => risk.Description.ToLower().Contains(searchTS.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Description });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchSubProcess(string searchSP)
        {
            APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
            var searchResults = dataContext.SubProcesses.ToList().FindAll(proc => proc.Name.ToLower().Contains(searchSP.ToLower()));
            var jsonResult = searchResults.Select(results => new { id = results.Id, name = results.Name });
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult StringLibrary_Read([DataSourceRequest]DataSourceRequest request)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                //var stringLibraries = dataContext.StringLibraries.Select(x => new StringLibraryViewModel(x)).ToList();
                IQueryable<StringLibraryViewModel> stringLibraries = dataContext.StringLibraries.Select(library => new StringLibraryViewModel
                {
                    BusinessProcessId= library.BusinessProcessId,
                    BusinessProcessName=library.BusinessProcess.Name,
                    ProcessId=library.ProcessId,
                    ProcessName=library.Process.Name,
                    SubProcessId=library.SubProcessId,
                    SubProcessName=library.SubProcess.Name,
                    ActivityId=library.ActivityId,
                    ActivityName=library.Activity.Name,
                    EnterpriseRiskId=library.EnterpriseRiskId,
                    EnterpriseRiskName=library.EnterpriseRisk.Name,
                    BusinessRiskId=library.BusinessRiskId,
                    BusinessRiskName=library.BusinessRisk.Name,
                    ExecutionRiskId=library.ExecutionRiskId,
                    ExecutionRiskName=library.ExecutionRisk.Name,
                    ControlId=library.ControlId,
                    ControlName=library.Control.Name,
                    TestObjectiveId=library.TestObjectiveId,
                    TestObjectiveName=library.TestObjective.Name,
                    TestId=library.TestId,
                    TestName=library.Test.Name
                });

                return(Json(stringLibraries.ToDataSourceResult(request),JsonRequestBehavior.AllowGet));
                //DataSourceResult result = stringLibraries.ToDataSourceResult(request);
                //return Json(stringLibraries);
            }
        }

        protected override object GetViewModel(StringLibrary result)
        {
            return new StringLibraryViewModel(result);
        }

        protected override bool TryUpdateViewModel(StringLibrary entity)
        {
            return TryUpdateModel(new StringLibrarySingleItemBuilderViewModel(entity));
        }

        protected override IEnumerable GetViewModel(IEnumerable<StringLibrary> results)
        {
            var stringLibraries = results.Select(stringLibrary => new StringLibraryViewModel(stringLibrary)).ToArray();
            return stringLibraries;
        }

    }
}
