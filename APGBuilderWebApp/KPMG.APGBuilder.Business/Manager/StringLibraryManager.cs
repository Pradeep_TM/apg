﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class StringLibraryManager : EntityManager<Entity.StringLibrary>
    {
        public StringLibraryManager() : base() { }
        public StringLibraryManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public StringLibraryManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
