﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace KPMG.APGBuilder.Web.Models
{
    public class BusinessProcessViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public BusinessProcessViewModel() { }
        public BusinessProcessViewModel(BusinessProcess businessProcess)
        {
            if (businessProcess != null)
            {
                this.Id = businessProcess.Id;
                this.Name = businessProcess.Name;
                this.Description = businessProcess.Description;
                this.IsActive = businessProcess.IsActive;
                if (businessProcess.BusinessProcessIndustryMappings != null && businessProcess.BusinessProcessIndustryMappings.Count > 0)
                {
                    SelectedIndustry = businessProcess.BusinessProcessIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    SubIndustry = string.Join(", ", businessProcess.BusinessProcessIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }
            this.businessProcess = businessProcess;
        }

        public BusinessProcess businessProcess { get; set; }
        public long Id { get; set; }
        [Required(ErrorMessage = "Business Process Name is Required...")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Business Process Description is Required...")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Select Business Process Status..."), DisplayName("Status")]
        public bool? IsActive { get; set; }


        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }


        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        [Required(ErrorMessage = "Select one Industry")]
        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long BPId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.BusinessProcessIndustryMappings
                              where industryMapping.SubIndustryId == industryId
                              && industryMapping.BusinessProcessId == BPId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping

    }
}