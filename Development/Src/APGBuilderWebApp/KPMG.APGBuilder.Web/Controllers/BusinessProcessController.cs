﻿using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using KPMG.APGBuilder.Web.Models;
using KGS.Framework.Web.Mvc.Telerik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.APGBuilder.Web.AppCode;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;


namespace KPMG.APGBuilder.Web.Controllers
{
    public class BusinessProcessController : GridEntityController<BusinessProcess>
    {
        public BusinessProcessController() : base(new BusinessProcessManager()) { }

        public override ActionResult Index()
        {
            IList<BusinessProcess> businessProcessList = Manager.GetList();
            ViewBag.Controller = APGConstants.BUSINESS_PROCESS;
            return View(GetViewModel(businessProcessList));
        }

        public override ActionResult Details(long id)
        {
            return View();
        }


        public override ActionResult Create()
        {
            return base.Create();
        }

        [HttpPost]
        public override ActionResult Create(FormCollection collection)
        {
            BusinessProcess bpObject = Manager.New();
            BusinessProcessViewModel viewModel = new BusinessProcessViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.CreatedBy = Convert.ToString(Session["UserName"]);
                bpObject.CreatedOn = DateTime.Now;
                Manager.Create(bpObject);
                viewModel.Id = bpObject.Id;
                CreateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }

        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        [HttpPost]
        public override ActionResult Edit(long id, FormCollection collection)
        {
            BusinessProcess bpObject = Manager.Read(id);
            BusinessProcessViewModel viewModel = new BusinessProcessViewModel();
            TryUpdateModel(viewModel);
            if (ModelState.IsValid)
            {
                bpObject.Name = viewModel.Name;
                bpObject.Description = viewModel.Description;
                bpObject.IsActive = viewModel.IsActive;
                bpObject.ModifiedBy = Convert.ToString(Session["UserName"]);
                bpObject.ModifiedOn = DateTime.Now;
                Manager.Update(bpObject);
                UpdateProjectForStandard(viewModel);
                return RedirectToAction(APGConstants.INDEX);
            }
            return View(viewModel);
        }


        public override ActionResult Delete(long id)
        {
            BusinessProcess bpObject = Manager.Read(id);
            bpObject.IsActive = false;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult Activate(long id)
        {
            BusinessProcess bpObject = Manager.Read(id);
            bpObject.IsActive = true;
            Manager.Update(bpObject);
            return RedirectToAction(APGConstants.INDEX);
        }

        public ActionResult BusinessProcess_Read([DataSourceRequest]DataSourceRequest request)
        {
            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                IQueryable<BusinessProcessViewModel> businessProcesses = dataContext.BusinessProcesses.Select(businessProcess => new BusinessProcessViewModel
                {
                    Id = businessProcess.Id,
                    Name = businessProcess.Name,
                    Description = businessProcess.Description,
                    IsActive = businessProcess.IsActive

                });
                return (Json(businessProcesses.ToDataSourceResult(request), JsonRequestBehavior.AllowGet));

            }
        }



        #region BusinessProcess Industry Mapping
        private void CreateProjectForStandard(BusinessProcessViewModel model)
        {
            if (model.SelectedIndustry != null && model.SelectedIndustry.Length > 0)
            {
                foreach (long selIndustry in model.SelectedIndustry)
                {
                    using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
                    {

                        dataContext.BusinessProcessIndustryMappings.InsertOnSubmit(new BusinessProcessIndustryMapping
                        {
                            BusinessProcessId = model.Id,
                            SubIndustryId = selIndustry,
                            CreatedBy = Convert.ToString(Session["UserName"]),
                            CreatedOn = DateTime.Now
                        });
                        dataContext.SubmitChanges();
                    }

                }
            }
        }

        private void UpdateProjectForStandard(BusinessProcessViewModel model)
        {
            var businessProcess = Manager.Read(model.Id);
            var existingIds = businessProcess.BusinessProcessIndustryMappings.Select(busProcess => busProcess.SubIndustryId).Distinct().ToList();

            if (model.SelectedIndustry == null)
            {
                // if the industry mapping is not available
                model.SelectedIndustry = new List<long>().ToArray();
            }

            var subIndustryToBeDeleted = existingIds.Except(model.SelectedIndustry).ToList();
            var subIndustryToBeAdded = model.SelectedIndustry.Except(existingIds).ToList();

            using (APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext())
            {
                if (subIndustryToBeDeleted.Count > 0)
                {
                    var subIndustryMappings = dataContext.BusinessProcessIndustryMappings.Where(busProcess => busProcess.BusinessProcessId.Equals(model.Id) && subIndustryToBeDeleted.Contains(busProcess.SubIndustryId)).ToList();
                    dataContext.BusinessProcessIndustryMappings.DeleteAllOnSubmit(subIndustryMappings);
                }

                if (subIndustryToBeAdded.Count > 0)
                {
                    subIndustryToBeAdded.ForEach(selIndustry =>
                    dataContext.BusinessProcessIndustryMappings.InsertOnSubmit(new BusinessProcessIndustryMapping
                    {
                        BusinessProcessId = model.Id,
                        SubIndustryId = selIndustry,
                        CreatedBy = Convert.ToString(Session["UserName"]),
                        CreatedOn = DateTime.Now
                    })
                    );
                }

                dataContext.SubmitChanges();
            }
        }

        #endregion BusinessProcess Industry Mapping


        protected override object GetViewModel(BusinessProcess result)
        {
            return new BusinessProcessViewModel(result);
        }

        protected override bool TryUpdateViewModel(BusinessProcess result)
        {
            return TryUpdateModel(new BusinessProcessViewModel(result));
        }

        private static System.Collections.Generic.IEnumerable<BusinessProcessViewModel> GetViewModel(IEnumerable<BusinessProcess> results, int a)
        {
            return results.Select(businessProcess => new BusinessProcessViewModel(businessProcess));
            //return businessProcesses;
        }

        protected override System.Collections.IEnumerable GetViewModel(IEnumerable<BusinessProcess> results)
        {
            var businessProcesses = results.Select(businessProcess => new BusinessProcessViewModel(businessProcess)).ToArray();
            return businessProcesses;

        }

    }
}
