﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using KPMG.APGBuilder.Business.Manager;

namespace KPMG.APGBuilder.Web.Models
{
    public class BusinessRiskViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();

        public BusinessRiskViewModel() { }

        public BusinessRiskViewModel(BusinessRisk businessRisk)
        {
            if(businessRisk!=null)
            {
                this.Id = businessRisk.Id;
                this.Name = businessRisk.Name;
                this.Description = businessRisk.Description;
                this.IsActive = businessRisk.IsActive;

                if (businessRisk.BusinessRiskIndustryMappings != null && businessRisk.BusinessRiskIndustryMappings.Count > 0)
                {
                    SelectedIndustry = businessRisk.BusinessRiskIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    SubIndustry = string.Join(", ", businessRisk.BusinessRiskIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }
            this.businessRisk = businessRisk;   

        }

        public BusinessRisk businessRisk { get; set; }
        public long Id { get; set; }
         
        [Required(ErrorMessage = "Business Risk Name is Required...")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Business Risk Description is Required...")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Select Business Risk Status..."), DisplayName("Status")]
        public bool? IsActive { get; set; }

        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long ERId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.BusinessRiskIndustryMappings
                              where industryMapping.SubIndustryId == industryId
                              && industryMapping.BusinessRiskId == ERId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping
    }
}