﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace KPMG.APGBuilder.Web.Models
{
    public class EnterpriseRiskViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();

        public EnterpriseRiskViewModel() { }

        public EnterpriseRiskViewModel(EnterpriseRisk enterpriseRisk)
        {
            if(enterpriseRisk != null)
            {
                this.Id = enterpriseRisk.Id;
                this.Name = enterpriseRisk.Name;
                this.Description = enterpriseRisk.Description;
                this.IsActive = enterpriseRisk.IsActive;

                if (enterpriseRisk.EnterpriseRiskIndustryMappings != null && enterpriseRisk.EnterpriseRiskIndustryMappings.Count > 0)
                {
                    SelectedIndustry = enterpriseRisk.EnterpriseRiskIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    SubIndustry = string.Join(", ", enterpriseRisk.EnterpriseRiskIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }

            this.enterpriseRisk = enterpriseRisk;


        }

            public EnterpriseRisk enterpriseRisk { get; set; }
            public long Id { get; set; }
            [Required(ErrorMessage = "Enterprise Risk Name is Required...")]
            public string Name { get; set; }

            [Required(ErrorMessage = "Enterprise Risk Description is Required...")]
            public string Description { get; set; }

            [Required(ErrorMessage = "Select Enterprise Risk Status..."), DisplayName("Status")]
            public bool? IsActive { get; set; }


        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        [Required(ErrorMessage = "Select one Industry")]
        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long ERId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.EnterpriseRiskIndustryMappings
                              where industryMapping.SubIndustryId == industryId
                              && industryMapping.EnterpriseRiskId == ERId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping

    }
}