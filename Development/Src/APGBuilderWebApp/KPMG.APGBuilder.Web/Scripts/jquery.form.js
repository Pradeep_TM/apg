﻿/**
* Enables or disables 1 element
*/
jQuery.fn.enableField = function (blnEnabled) {
	oElement = this[0];

	//
	// Set correct thingie
	if (!blnEnabled) {
		//
		// Disable element
		oElement.disabledOld = (oElement.disabled || false);
		oElement.disabled = true;

		//
		// Class toevoegen
		this.addClass('disabled');
	} else {
		oElement.disabled = (oElement.disabledOld || '');
		oElement.disabledOld = '';

		//
		// Class toevoegen
		this.removeClass('disabled');
	}
};

/**
* Enables or disables the entire form
* Assumes the original form is enabled before calling this function, thus remembering
* the state the element was in the first time this function was called.
*/
jQuery.fn.formAble = function (blnEnabled) {
	jQuery(":input", this).each(function () {
		jQuery(this).enableField(blnEnabled);
	});
};


