﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace KPMG.APGBuilder.Web.Models
{
    public class ProcessViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public ProcessViewModel() { }
        public ProcessViewModel(Process process)
        {
            if (process != null)
            {
                this.Id = process.Id;
                this.Name = process.Name;
                this.Description = process.Description;
                this.IsActive = process.IsActive;
                if (process.ProcessIndustryMappings != null && process.ProcessIndustryMappings.Count > 0)
                {
                    this.SelectedIndustry = process.ProcessIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    this.SubIndustry = string.Join(", ", process.ProcessIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }
            this.process = process;
        }

        public Process process { get; set; }
        public long Id { get; set; }
        [Required(ErrorMessage = "Process Name is Required...")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Process Description is Required...")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Select Process Status..."), DisplayName("Status")]
        public bool? IsActive { get; set; }

        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }


        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        [Required(ErrorMessage = "Select one Industry")]
        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long processId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.ProcessIndustryMappings
                              where industryMapping.SubIndustryId == industryId
                              && industryMapping.ProcessId == processId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping
    }
}