﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KPMG.APGBuilder.Web.Models
{
    public class SubProcessViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public SubProcessViewModel() { }
        public SubProcessViewModel(SubProcess subProcess)
        {
            if (subProcess != null)
            {
                this.Id = subProcess.Id;
                this.Name = subProcess.Name;
                this.Description = subProcess.Description;
                this.IsActive = subProcess.IsActive;
                if (subProcess.SubProcessIndustryMappings != null && subProcess.SubProcessIndustryMappings.Count > 0)
                {
                    SelectedIndustry = subProcess.SubProcessIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    SubIndustry = string.Join(", ", subProcess.SubProcessIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }

            this.SubProcess = subProcess;
        }


        public SubProcess SubProcess { get; set; }
        public long Id { get; set; }
        [Required(ErrorMessage="Please enter Sub Process name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Please select IsActive")]
        public bool? IsActive { get; set; }


        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }


        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long subProcessId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.SubProcessIndustryMappings
                                  where industryMapping.SubIndustryId == industryId
                                  && industryMapping.SubProcessId == subProcessId
                                  select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
               IsSubIndustry = true;
            }
            
            return IsSubIndustry;

        }

  
        #endregion Industry Mapping
    }
}