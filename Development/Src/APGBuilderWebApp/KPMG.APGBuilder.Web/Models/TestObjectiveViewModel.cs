﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace KPMG.APGBuilder.Web.Models
{
    public class TestObjectiveViewModel : IIndustySelector
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();

        public TestObjectiveViewModel() { }

        public TestObjectiveViewModel(TestObjective testObjective)
        {
            if (testObjective != null)
            {
                this.Id = testObjective.Id;
                this.Name = testObjective.Name;
                this.Description = testObjective.Description;
                this.IsActive = testObjective.IsActive;

                if (testObjective.TestObjectiveIndustryMappings != null && testObjective.TestObjectiveIndustryMappings.Count > 0)
                {
                    SelectedIndustry = testObjective.TestObjectiveIndustryMappings.Select(x => x.SubIndustryId).ToArray();
                    SubIndustry = string.Join(", ", testObjective.TestObjectiveIndustryMappings.Select(x => x.SubIndustry.Name).ToList());
                }
            }
            this.testObjective = testObjective;
        }


        public TestObjective testObjective { get; set; }
        public long Id { get; set; }

        [Required(ErrorMessage = "Test Objective Name is Required...")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Test Objective Description is Required...")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Select Test Objective Status..."), DisplayName("Status")]
        public bool? IsActive { get; set; }

        //public bool IsActive { get; set; }
        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        #region Industry Mapping

        private IList<Industry> _industryList;
        public IList<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    _industryList = new IndustryManager().GetList();
                }
                return _industryList;
            }
        }

        private IList<SubIndustry> _subIndustryList;
        public IList<SubIndustry> SubIndustryList
        {
            get
            {
                if (_subIndustryList == null)
                {
                    _subIndustryList = new SubIndustryManager().GetList();
                }
                return _subIndustryList;
            }
        }


        public string SubIndustry { get; set; }

        public long[] SelectedIndustry { get; set; }

        public bool HasSubIndustryExist(long industryId, long ERId)
        {
            bool IsSubIndustry = false;
            var hasIndustry = from industryMapping in dataContext.TestObjectiveIndustryMappings
                              where industryMapping.SubIndustryId == industryId
                              && industryMapping.TestObjectiveId == ERId
                              select industryMapping;
            if (hasIndustry != null && hasIndustry.ToList().Count > 0)
            {
                IsSubIndustry = true;
            }

            return IsSubIndustry;

        }


        #endregion Industry Mapping
    }
}