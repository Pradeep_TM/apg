﻿using KGS.Framework.Business;
using KGS.Framework.Data;
using KPMG.APGBuilder.Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPMG.APGBuilder.Business.Manager
{
    public class APGManager : EntityManager<APG>
    {
        public APGManager() : base() { }
        public APGManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public APGManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
