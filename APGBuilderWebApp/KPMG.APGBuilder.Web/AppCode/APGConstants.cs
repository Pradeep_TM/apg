﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPMG.APGBuilder.Web.AppCode
{
    public static class APGConstants
    {
        public const string USER_NAME = "UserName";
        public const string INDEX = "Index";
        public const string SUB_PROCESS_TITLE = "APG Builder - Sub Process List";
        public const string LAYOUT_PATH = "~/Views/Shared/_Layout.cshtml";
        public const string SUB_PROCESS_LIST = "Sub Process List";
        public const string CREATE_NEW = "Create New";
        public const string CREATE = "Create";
        public const string EDIT_BUSINESS_PROCESS = "Edit Business Process";
        public const string SUB_PROCESS = "SubProcess";
        public const string BUSINESS_PROCESS = "BusinessProcess";
        public const string CONTROL = "Control";
        public const string EXECUTION_RISK = "ExecutionRisk";
        public const string PROCESS = "Process";
        public const string LOB="Lob";
        public const string INDUSTRY="Industry";
        public const string SUB_INDUSTRY = "SubIndustry";
        public const string ACTIVITY = "Activity";
        public const string ENTERPRISE_RISK = "EnterpriseRisk";
        public const string BUSINESS_RISK = "BusinessRisk";
        public const string TEST_OBJECTIVE = "TestObjective";
        public const string TEST = "Test";
        public const string SELECTED_CSS = "class=\"selected\"";
    }

    public class APGUtility
    {
        public static string ApplySelectedClass(object currentCntrolr, string controllerName)
        {
            return ((string)currentCntrolr == controllerName) ? APGConstants.SELECTED_CSS : string.Empty;
        }           
    }
}

