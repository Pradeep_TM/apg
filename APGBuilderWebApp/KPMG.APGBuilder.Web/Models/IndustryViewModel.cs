﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace KPMG.APGBuilder.Web.Models
{
    public class IndustryViewModel
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public IndustryViewModel() { }
        public IndustryViewModel(Industry industry)
        {
            if (industry != null)
            {
                this.Id = industry.Id;
                this.Name = industry.Name;
                this.Description = industry.Description;
                this.IsActive = industry.IsActive;
                this.SelectedLobId = industry.LOBId;
            }
            this.industry = industry;
        }


        public Industry industry { get; set; }
        public long Id { get; set; }
        [Required(ErrorMessage = "Industry Name is Required...")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Industry Description is Required...")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Select Industry Status..."), DisplayName("Status")]
        public bool? IsActive { get; set; }

        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        //[Required(ErrorMessage = "Select one LOB..."),DisplayName("LOB")]
       // [RegularExpression("/^(!?--SELECT--)$/",ErrorMessage="Select LOB")]
        public long SelectedLobId { get; set; }

        private Dictionary<long, string> lobDict = new Dictionary<long, string>();

        public Dictionary<long, string> LobDict
        {
            get
            {
                foreach (var lob in this.LobList)
                {
                    if (!lobDict.ContainsKey(lob.Id))
                    {
                        lobDict.Add(lob.Id, lob.Name);
                    }
                }
                return lobDict;
            }
        }

        private IList<LOB> _lobList;
        public IList<LOB> LobList
        {
            get
            {
                if (_lobList == null)
                {
                    _lobList = new LOBManager().GetList();
                    // _lobList = new LOBManager().GetList().Where(lob=>lob.IsActive==true).ToList();
                }

                return _lobList;
            }
        }




    }
}