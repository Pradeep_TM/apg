﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class TestStepManager : EntityManager<Entity.TestStep>
    {
        
        public TestStepManager() : base() { }
        public TestStepManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public TestStepManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
