﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.APGBuilder.Business.Entity;
using KPMG.APGBuilder.Business.Manager;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace KPMG.APGBuilder.Web.Models
{
    public class LOBViewModel
    {
        APGBuilderModelDataContext dataContext = new APGBuilderModelDataContext();
        public LOBViewModel() { }
        public LOBViewModel(LOB Lob)
        {
            if(Lob!=null)
            {
                this.Id = Lob.Id;
                this.Name = Lob.Name;
                this.Description = Lob.Description;
                this.IsActive = Lob.IsActive;
            }
            this.lob = Lob;
        }

        public LOB lob { get; set; }
        public long Id { get; set; }
        
        [Required(ErrorMessage = "LOB Name is Required...")]
        public string Name { get; set; }
        [Required(ErrorMessage = "LOB Description is Required...")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Select LOB Status..."),DisplayName("Status")]
        public bool? IsActive { get; set; }

        public List<bool> ActiveList { get; set; }
        private SelectList isActiveList;
        public SelectList IsActiveList
        {
            get
            {
                ActiveList = new List<bool>();
                ActiveList.Add(true);
                ActiveList.Add(false);
                isActiveList = new SelectList(ActiveList, IsActive);
                return isActiveList;
            }
        }

        //private IList<LOB> _lobList;
        //public IList<LOB> LobList
        //{
        //    get
        //    {
        //        if(_lobList==null)
        //        {
        //            _lobList = new LOBManager().GetList();
        //        }
        //        return _lobList;
        //    }
        //}


    }
}