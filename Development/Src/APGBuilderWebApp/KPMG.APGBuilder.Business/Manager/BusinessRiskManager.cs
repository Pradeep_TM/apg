﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KGS.Framework.Business;
using KGS.Framework.Data;

namespace KPMG.APGBuilder.Business.Manager
{
    public class BusinessRiskManager : EntityManager<Entity.BusinessRisk>
    {
        public BusinessRiskManager() : base() { }
        public BusinessRiskManager(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        public BusinessRiskManager(string name, bool useSharedUnitOfWork) : base(name, useSharedUnitOfWork) { }
    }
}
